#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "analyst.h"
#include "encoding.h"
#include "seed.h"
#include "sort.h"
#include "pyash.h"

extern inline void code_ACC_consonant_one(const uint8_t type,
                                          const uint8_t consonant_one,
                                          uint16_t *number) {
  uint8_t i, consonant_number = CONSONANT_ONE_ENCODE_LONG;
  assert(consonant_Q((char)consonant_one) == TRUE);
  if (consonant_one != 0 && type != SHORT_ROOT) {
    for (i = 0; i < CONSONANT_ONE_ENCODE_LONG; i++) {
      if (consonant_one_code_group[i][0] == consonant_one) {
        consonant_number = consonant_one_code_group[i][1];
        assert(consonant_number < CONSONANT_ONE_ENCODE_LONG);
        if (type == LONG_ROOT) {
          *number = consonant_number;
          break;
        } else if (type == LONG_GRAMMAR) {
          *number |= (uint16_t)(consonant_number << BANNER_THICK);
          break;
        } else if (type == SHORT_GRAMMAR) {
          *number |= (uint16_t)(consonant_number << CONSONANT_ONE_THICK);
          break;
        }
      }
    }
  }
  assert(consonant_number != CONSONANT_ONE_ENCODE_LONG);
}

uint8_t vector_long_find(uint16_t number) {
  if (number < 5)
    return (uint8_t)number;
  if (number <= 8)
    return 8;
  if (number <= 16)
    return 16;
  return 0;
}

uint16_t vector_code(uint8_t vector_long) {
  // returns greater than MAXIMUM_VECTOR_CODE if invalid
  if (vector_long == 1)
    return VECTOR_THICK_1;
  if (vector_long == 2)
    return VECTOR_THICK_2;
  if (vector_long == 3)
    return VECTOR_THICK_3;
  if (vector_long == 4)
    return VECTOR_THICK_4;
  if (vector_long == 8)
    return VECTOR_THICK_8;
  if (vector_long == 16)
    return VECTOR_THICK_16;
  assert(1 == 0);
  return MAXIMUM_VECTOR_CODE + 1;
}

extern inline void code_ACC_consonant_two(const uint8_t type,
                                          const uint8_t consonant_two,
                                          uint16_t *number) {
  uint8_t i, consonant_number = CONSONANT_TWO_ENCODE_LONG;
  uint16_t start_number = *number;
  // printf("n %04X, t %X c2 %c \n", (uint) *number,
  //       (uint) type, (char) consonant_two);
  assert(consonant_Q((char)consonant_two) == TRUE);
  if (consonant_two != 0 && type == SHORT_ROOT) {
    for (i = 0; i < CONSONANT_ONE_ENCODE_LONG; i++) {
      if (consonant_one_code_group[i][0] == consonant_two) {
        consonant_number = consonant_one_code_group[i][1];
        assert(consonant_number < CONSONANT_ONE_ENCODE_LONG);
        *number |= (uint16_t)(consonant_number << BANNER_THICK);
        break;
      }
    }
  }
  if (consonant_two != 0 && type != SHORT_ROOT && type != SHORT_GRAMMAR) {
    for (i = 0; i < CONSONANT_TWO_ENCODE_LONG; i++) {
      if (consonant_two_code_group[i][0] == consonant_two) {
        consonant_number = consonant_two_code_group[i][1];
        assert(consonant_number < CONSONANT_TWO_ENCODE_LONG);
        if (type == LONG_ROOT) {
          // printf("C2LR cn %04X\n", (uint) consonant_number);
          // printf("C2LR %04X\n", (uint) consonant_number <<
          //       (CONSONANT_ONE_THICK));
          *number |= (uint16_t)(consonant_number << CONSONANT_ONE_THICK);
          break;
        } else if (type == LONG_GRAMMAR) {
          *number |= (uint16_t)(consonant_number
                                << (BANNER_THICK + CONSONANT_ONE_THICK));
          break;
        }
      }
    }
  }
  assert(consonant_number == 0 || *number != start_number);
  assert(consonant_number != CONSONANT_ONE_ENCODE_LONG);
}

extern inline void code_ACC_type(const char *word,
                                 const uint8_t ACC_GEN_magnitude, uint8_t *type,
                                 uint16_t *number) {
  assert(word != NULL);
  assert(ACC_GEN_magnitude <= WORD_LONG);
  // printf("w %s\n", word);
  if (ACC_GEN_magnitude == 2 || ACC_GEN_magnitude == 3) {
    *type = SHORT_GRAMMAR;
    *number = 30;
  } else if (ACC_GEN_magnitude == 4 && word[3] == 'h') {
    *type = LONG_GRAMMAR;
    *number = 7;
  } else if (ACC_GEN_magnitude == 4 && word[0] == 'h') {
    *type = SHORT_ROOT;
    *number = 0;
  } else if (ACC_GEN_magnitude == 4) {
    *type = LONG_ROOT;
    *number = 0;
  } else if (ACC_GEN_magnitude == 5 && word[0] == 'h') {
    *type = SHORT_ROOT;
    *number = 0;
  } else if (ACC_GEN_magnitude == 5 && word[4] == 'h') {
    *type = LONG_GRAMMAR;
    *number = 7;
  } else if (ACC_GEN_magnitude == 5) {
    *type = LONG_ROOT;
    *number = 0;
  } else {
    *type = ERROR_BINARY;
    *number = 0;
  }
  assert(*type != ERROR_BINARY);
}
#define code_exit                                                              \
  *DAT_number = 0;                                                             \
  return;
void word_number_encode(const uint8_t ACC_GEN_magnitude, const char *word,
                        uint16_t *DAT_number) {
  /* Algorithm:
      TEL set ACC NUM zero DAT number DEO
      identify type of word
      if ACC word class ESS short root word
      then TEL mark ACC first three binary DAT NUM zero
      else-if ACC word class ESS long grammar word
      then TEL mark ACC first three binary DAT NUM one
      else-if ACC word class ESS short grammar word
      then TEL mark ACC first byte DAT NUM thirty
      UNQ glyph INE indexFinger POSC word QUOT process
      ATEL search ACC code table BEN glyph DAT glyph number DEO
      TEL multiply ACC glyph number INS indexFinger DAT indexFinger number DEO
      TEL add ACC indexFinger number DAT number DEO
      process QUOT DEO
  */
  uint8_t consonant_one = 0, consonant_two = 0, vowel = 0, tone = 0,
          consonant_three = 0, type = LONG_ROOT;
  uint16_t number = 0;
  assert(ACC_GEN_magnitude > 0);
  assert(ACC_GEN_magnitude <= WORD_LONG);
  assert(word != NULL);
  assert(DAT_number != NULL);
  code_ACC_type(word, ACC_GEN_magnitude, &type, &number);
  // printf("type %X\n", (uint) number);
  /* TEL fill ACC glyph variable PL */
  consonant_one = (uint8_t)word[0];
  if (consonant_Q((char)consonant_one) == FALSE) {
    code_exit;
  }
  assert(consonant_Q((char)consonant_one) == TRUE);
  if (ACC_GEN_magnitude == 2) {
    vowel = (uint8_t)word[1];
    if (vowel_Q((char)vowel) == FALSE) {
      code_exit;
    }
    assert(vowel_Q((char)vowel) == TRUE);
  } else if (ACC_GEN_magnitude == 3) {
    vowel = (uint8_t)word[1];
    tone = (uint8_t)word[2];
    if (vowel_Q((char)vowel) == FALSE || tone_Q((char)tone) == FALSE) {
      code_exit;
    }
    assert(vowel_Q((char)vowel) == TRUE);
    assert(tone_Q((char)tone) == TRUE);
  } else if (ACC_GEN_magnitude == 4) {
    consonant_two = (uint8_t)word[1];
    vowel = (uint8_t)word[2];
    consonant_three = (uint8_t)word[3];
    if (consonant_Q((char)consonant_two) == FALSE ||
        vowel_Q((char)vowel) == FALSE ||
        consonant_Q((char)consonant_three) == FALSE) {
      code_exit;
    }
    assert(consonant_Q((char)consonant_two) == TRUE);
    assert(vowel_Q((char)vowel) == TRUE);
    assert(consonant_Q((char)consonant_three) == TRUE);
  } else if (ACC_GEN_magnitude == 5) {
    consonant_two = (uint8_t)word[1];
    vowel = (uint8_t)word[2];
    tone = (uint8_t)word[3];
    consonant_three = (uint8_t)word[4];
    if (consonant_Q((char)consonant_two) == FALSE ||
        vowel_Q((char)vowel) == FALSE || tone_Q((char)tone) == FALSE ||
        consonant_Q((char)consonant_three) == FALSE) {
      code_exit;
    }
    assert(consonant_Q((char)consonant_two) == TRUE);
    assert(vowel_Q((char)vowel) == TRUE);
    assert(tone_Q((char)tone) == TRUE);
    assert(consonant_Q((char)consonant_three) == TRUE);
  }
  if (consonant_one != 0 && type != SHORT_ROOT) {
    code_ACC_consonant_one(type, consonant_one, &number);
    // printf("c1 %X\n", (uint) number);
  }
  if (consonant_two != 0) {
    code_ACC_consonant_two(type, consonant_two, &number);
    // printf("c2 %X\n", (uint) number);
  }
  code_ACC_vowel(type, vowel, &number);
  // printf("v %X\n", (uint) number);
  if (tone != 0) {
    code_ACC_tone(type, tone, &number);
    // printf("tone %X\n", (uint) number);
  } else {
    code_ACC_tone(type, 'M', &number);
  }
  if (consonant_three != 0 && type != LONG_GRAMMAR) {
    code_ACC_consonant_three(type, consonant_three, tone, &number);
    // printf("c3 %X\n", (uint) number);
  }
  *DAT_number = number;
}

extern inline void code_ACC_vowel(const uint8_t type, const uint8_t vowel,
                                  uint16_t *number) {
  uint8_t i, vowel_number = VOWEL_ENCODE_LONG;
  // uint16_t start_number = *number;
  // printf("n %04X, t %X v %c \n", (uint) *number,
  //       (uint) type, (char) vowel);
  assert(vowel_Q((char)vowel) == TRUE);
  if (vowel != 0) {
    for (i = 0; i < VOWEL_ENCODE_LONG; i++) {
      if (vowel_code_group[i][0] == vowel) {
        vowel_number = vowel_code_group[i][1];
        assert(vowel_number < VOWEL_ENCODE_LONG);
        if (type == LONG_ROOT) {
          // printf("VLR %04X\n", (uint) vowel_number << (
          //       CONSONANT_ONE_THICK + CONSONANT_TWO_THICK));
          *number |= (uint16_t)(vowel_number
                                << (CONSONANT_ONE_THICK + CONSONANT_TWO_THICK));
          break;
        } else if (type == SHORT_ROOT) {
          *number |=
              (uint16_t)(vowel_number << (BANNER_THICK + CONSONANT_ONE_THICK));
          break;
        } else if (type == LONG_GRAMMAR) {
          // printf("VLG %04X\n", (uint) vowel_number << (BANNER_THICK +
          //       CONSONANT_ONE_THICK + CONSONANT_TWO_THICK));
          *number |=
              (uint16_t)(vowel_number << (BANNER_THICK + CONSONANT_ONE_THICK +
                                          CONSONANT_TWO_THICK));
          break;
        } else if (type == SHORT_GRAMMAR) {
          *number |= (uint16_t)(vowel_number << (CONSONANT_ONE_THICK * 2));
          break;
        }
      }
    }
  }
  // assert(vowel_number == 0 || *number != start_number);
  assert(vowel_number != VOWEL_ENCODE_LONG);
}

extern inline void code_ACC_tone(const uint8_t type, const uint8_t tone,
                                 uint16_t *number) {
  uint8_t i, tone_number = TONE_ENCODE_LONG;
  uint16_t start_number = *number;
  // printf("n %04X, t %X tn %c \n", (uint) *number,
  //       (uint) type, (char) tone);
  assert(tone_Q((char)tone) == TRUE);
  if (tone != 0) {
    for (i = 0; i < TONE_ENCODE_LONG; i++) {
      if (tone_code_group[i][0] == tone) {
        tone_number = tone_code_group[i][1];
        if (type == LONG_ROOT) {
          // printf("TLR %X\n", (uint)(tone_number <<
          //       (CONSONANT_ONE_THICK + CONSONANT_TWO_THICK + VOWEL_THICK)));
          *number |=
              (uint16_t)(tone_number << (CONSONANT_ONE_THICK +
                                         CONSONANT_TWO_THICK + VOWEL_THICK));
          break;
        } else if (type == SHORT_ROOT) {
          *number |=
              (uint16_t)(tone_number
                         << (BANNER_THICK + CONSONANT_ONE_THICK + VOWEL_THICK));
          break;
        } else if (type == LONG_GRAMMAR) {
          *number |=
              (uint16_t)(tone_number << (BANNER_THICK + CONSONANT_ONE_THICK +
                                         CONSONANT_TWO_THICK + VOWEL_THICK));
          break;
        } else if (type == SHORT_GRAMMAR) {
          *number |= (uint16_t)(tone_number
                                << (CONSONANT_ONE_THICK * 2 + VOWEL_THICK));
          break;
        }
      }
    }
  }
  assert(tone_number == 0 || *number != start_number);
  assert(tone_number != TONE_ENCODE_LONG);
}

extern inline void code_ACC_consonant_three(const uint8_t type,
                                            const uint8_t consonant_three,
                                            const uint8_t tone,
                                            uint16_t *number) {
  uint8_t i, consonant_number = CONSONANT_THREE_ENCODE_LONG;
  uint16_t start_number = *number;
  // printf("n %04X, t %X c %c  tn %c\n", (uint) *number,
  //       (uint) type, (char) consonant_three, (char) tone);
  if (consonant_three != 0 && type != SHORT_GRAMMAR && type != LONG_GRAMMAR) {
    for (i = 0; i < CONSONANT_THREE_ENCODE_LONG; i++) {
      if (consonant_three_code_group[i][0] == consonant_three) {
        consonant_number = consonant_three_code_group[i][1];
        if (type == LONG_ROOT && tone == 0) {
          // printf("C3LR %04X \n", (uint) (consonant_number
          //               << (CONSONANT_ONE_THICK + CONSONANT_TWO_THICK +
          //                  VOWEL_THICK + TONE_THICK)));
          *number |= (uint16_t)(consonant_number
                                << (CONSONANT_ONE_THICK + CONSONANT_TWO_THICK +
                                    VOWEL_THICK + TONE_THICK));
          break;
        } else if (type == SHORT_ROOT && tone == 0) {
          // printf("SR %04X \n", (uint) (consonant_number
          //               << (BANNER_THICK + CONSONANT_ONE_THICK +
          //               VOWEL_THICK)));
          *number |= (uint16_t)(consonant_number
                                << (BANNER_THICK + CONSONANT_ONE_THICK +
                                    VOWEL_THICK + TONE_THICK));
          break;
        } else if (type == LONG_ROOT && tone != 0) {
          *number |= (uint16_t)(consonant_number
                                << (CONSONANT_ONE_THICK + CONSONANT_TWO_THICK +
                                    VOWEL_THICK + TONE_THICK));
          break;
        } else if (type == SHORT_ROOT && tone != 0) {
          *number |= (uint16_t)(consonant_number
                                << (BANNER_THICK + CONSONANT_ONE_THICK +
                                    VOWEL_THICK + TONE_THICK));
          break;
        }
      }
    }
  }
  assert(consonant_number == 0 || *number != start_number);
  assert(consonant_number != CONSONANT_THREE_ENCODE_LONG);
}

void number_word_to_number(const uint16_t number_word, uint8_t *number) {
  // returns greater than MAXIMUM_WORD_NUMBER if invalid,
  // so be sure to assert(number <= MAXIMUM_WORD_NUMBER); afterwards;
  switch (number_word) {
  case zero_WORD:
    break;
  case one_WORD:
    *number = 1;
    break;
  case two_WORD:
    *number = 2;
    break;
  case three_WORD:
    *number = 3;
    break;
  case four_WORD:
    *number = 4;
    break;
  case five_WORD:
    *number = 5;
    break;
  case six_WORD:
    *number = 6;
    break;
  case seven_WORD:
    *number = 7;
    break;
  case eight_WORD:
    *number = 8;
    break;
  case nine_WORD:
    *number = 9;
    break;
  case ten_WORD:
    *number = 0xA;
    break;
  case eleven_WORD:
    *number = 0xB;
    break;
  case twelve_WORD:
    *number = 0xC;
    break;
  case thirteen_WORD:
    *number = 0xD;
    break;
  case fourteen_WORD:
    *number = 0xE;
    break;
  case fifteen_WORD:
    *number = 0xF;
    break;
  case sixteen_WORD:
    *number = 0x10;
    break;
  case seventeen_WORD:
    *number = 0x11;
    break;
  default:
    *number = MAXIMUM_WORD_NUMBER + 1;
    break;
  }
}
void retrospective_phrase_indexFinger_found(
    const uint16_t tablet_indexFinger, const uint8_t series_long,
    const v16us *tablet, uint16_t *retrospective_phrase_indexFinger) {
  assert(TABLET_LONG * series_long > tablet_indexFinger);
  assert(tablet != NULL);
  uint16_t binary_phrase_list = 0;
  uint8_t indicator = 0;
  uint8_t series_indexFinger = (uint8_t)tablet_indexFinger / TABLET_LONG;
  uint8_t champion = FALSE;
  printf("%s:%d:%s\n\t", __FILE__, __LINE__, __FUNCTION__);
  uint8_t vector_indexFinger = 0;
  for (; series_indexFinger < series_long; --series_indexFinger) {
    v16us_print(tablet[series_indexFinger]);
    binary_phrase_list = (uint16_t)tablet[series_indexFinger].s0;
    // printf("\t binary_phrase_list 0x%04X\n\t ", binary_phrase_list);
    indicator = binary_phrase_list & 1;
    vector_indexFinger =
        (tablet_indexFinger - series_indexFinger * TABLET_LONG) % TABLET_LONG;
    // printf("vector_indexFinger 0x%X, ", vector_indexFinger);
    for (; vector_indexFinger < TABLET_LONG; --vector_indexFinger) {
      // printf("indicator 0x%X, ", indicator);
      printf("indexFinger 0x%X, ", vector_indexFinger);
      printf("tidbit 0x%X,\n\t ",
             (binary_phrase_list >> vector_indexFinger) & 1);

      if (((binary_phrase_list >> vector_indexFinger) & 1) == indicator) {
        *retrospective_phrase_indexFinger =
            vector_indexFinger + series_indexFinger * TABLET_LONG;
        // printf("champion 0x%X, ", vector_indexFinger);
        champion = TRUE;
        break;
      }
    }
    if (champion == TRUE) {
      break;
    }
    *retrospective_phrase_indexFinger = 0;
  }
  // printf("\n");
}
extern void convert_last_number_to_quote(uint16_t *terminator_indexFinger,
                                         const uint8_t series_long,
                                         v16us *tablet) {
  uint16_t tablet_indexFinger = 0;
  uint8_t number_indexFinger = 0;
  uint64_t number = 0;
  uint8_t finish = FALSE;
  // uint16_t indicator_list = (uint16_t)tablet[0].s0;
  // uint8_t indicator = indicator_list & 1;
  uint16_t retrospective_phrase_indexFinger = 0;
  uint16_t quote_indexFinger = 0;
  uint16_t quote_word = 0;
  uint8_t letter_number = 0;
  assert(*terminator_indexFinger > 0);
  assert(tablet != NULL);
  tablet_indexFinger = *terminator_indexFinger;
  retrospective_phrase_indexFinger_found(tablet_indexFinger, series_long,
                                         tablet,
                                         &retrospective_phrase_indexFinger);
  uint16_t number_word = 0;
  // see how long the number is
  for (; tablet_indexFinger > 0; --tablet_indexFinger) {
    printf("%s:%d tablet_indexFinger 0x%X\n", __FILE__, __LINE__,
           tablet_indexFinger);
    number_word = tablet_retrospective_word_read(
        tablet_indexFinger, series_long, tablet, &tablet_indexFinger);
    number_word_to_number(number_word, &letter_number);
    if (letter_number <= MAXIMUM_WORD_NUMBER) { // if found
      printf("%s:%d number detected 0x%X\n", __FILE__, __LINE__, number_word);
      number |= (uint64_t)(letter_number << number_indexFinger * 4);
      ++number_indexFinger;
    } else {
      finish = TRUE;
    }
    printf("%s:%d indexFinger 0x%X TIF 0x%X\n", __FILE__, __LINE__,
           number_indexFinger, tablet_indexFinger);
    if (finish == TRUE) {
      //  printf("%s:%d finishing\n", __FILE__,__LINE__);
      // if (number_indexFinger > 1) {
      //  retrospective_phrase_indexFinger -= number_indexFinger - 1;
      //}
      printf("%s:%d:%s\n", __FILE__, __LINE__, __FUNCTION__);
      tablet_print(series_long, tablet);
      printf("\tretrospective_phrase_indexFinger 0x%X, *terminator_indexFinger "
             " 0x%X, number_indexFinger 0x%X\n",
             retrospective_phrase_indexFinger, *terminator_indexFinger,
             number_indexFinger);
      assert(retrospective_phrase_indexFinger ==
             *terminator_indexFinger - number_indexFinger);
      break;
    }
  }
  /* set up quote and number if necessary (number_indexFinger > 2)*/
  // printf("number %X, tablet_indexFinger %X, number_indexFinger %X\n",
  // (uint)number,
  //       (uint)tablet_indexFinger, (uint)number_indexFinger);
  // printf("number %lX\n", number);
  if (number <= 0xFFFF) {
    quote_word = QUOTE_DENOTE |
                 SIXTEEN_TIDBIT_SCALAR_THICK << SCALAR_THICK_BEGIN |
                 UINT_SORT_DENOTE << SORT_DENOTE_BEGIN;
    printf("%s:%d ", __FILE__, __LINE__);
    v16us_print(tablet[0]);
    printf("\tRPI %X\n", retrospective_phrase_indexFinger);
    quote_indexFinger = (uint16_t)retrospective_phrase_indexFinger + 1;
    tablet_word_write(quote_indexFinger, (uint16_t)(quote_word), series_long,
                      tablet, &quote_indexFinger);
    ++quote_indexFinger;
    tablet_word_write(quote_indexFinger, (uint16_t)(number), series_long,
                      tablet, &quote_indexFinger);
    *terminator_indexFinger = (uint8_t)(quote_indexFinger);
    // printf("RPI %X\n", retrospective_phrase_indexFinger);
  }
  assert(number_indexFinger <= 4);
}
void text_encoding(const uint16_t max_text_magnitude, const char *text,
                   uint16_t *tablet_series_long, v16us *tablet,
                   uint16_t *text_remainder) {
  /* find end of independentClause for each,
    then pass each independentClause to independentClause code,
    return the result */
  const uint16_t max_tablet_series_long = *tablet_series_long;
  uint8_t independentClause_tablet_series_long = MAX_INDEPENDENTCLAUSE_TABLET;
  uint16_t text_indexFinger = 0;
  assert(text != NULL);
  assert(max_text_magnitude != 0);
  assert(tablet != NULL);
  assert(tablet_series_long != NULL);
  assert(*tablet_series_long * TABLET_BYTE_LONG <= MAX_WRITE_MEMORY);
  assert(text_remainder != NULL);
  *tablet_series_long = 0;
  *text_remainder = (uint16_t)strlen(text); // max_text_magnitude;
  for (; *tablet_series_long < max_tablet_series_long;) {
    if ((max_tablet_series_long - *tablet_series_long) <
        MAX_INDEPENDENTCLAUSE_TABLET) {
      independentClause_tablet_series_long =
          (uint8_t)(max_tablet_series_long - *tablet_series_long);
    } else {
      independentClause_tablet_series_long =
          (uint8_t)MAX_INDEPENDENTCLAUSE_TABLET;
    }
    printf("%s:%d:%s\n\t", __FILE__, __LINE__, __FUNCTION__);
    v16us_print(tablet[*tablet_series_long]);
    independentClause_encoding(*text_remainder, text + text_indexFinger,
                               &independentClause_tablet_series_long,
                               &tablet[*tablet_series_long], text_remainder);
    *tablet_series_long =
        (uint16_t)(*tablet_series_long + independentClause_tablet_series_long);
    printf("%s:%d:%s\n", __FILE__, __LINE__, __FUNCTION__);
    printf("\tic_long 0x%X", independentClause_tablet_series_long);
    printf(" tablet_series_long 0x%X\n", *tablet_series_long);
    tablet_print((uint8_t)*tablet_series_long, tablet);
    if (*text_remainder <= 2) { // if remainder is empty
      break;
    }
    text_indexFinger = (uint16_t)(max_text_magnitude - *text_remainder);
    // printf("ct text_indexFinger %X, text `%s', tablet_series_long 0x%X\n",
    //       (uint)text_indexFinger, text + text_indexFinger,
    //       *tablet_series_long);
    // printf("ct text_remainder %X\n",(uint)*text_remainder);
    // printf("ct tablet_series_long %X\n",(uint)*tablet_series_long);
  }
  // printf("ctf text_remainder %X\n",(uint)*text_remainder);
  // printf("ctf tablet_series_long %X\n",(uint)*tablet_series_long);
}
void word_series_encode(const uint16_t text_long, const char *text,
                        uint16_t *word_series_long, uint16_t *word_series) {
  assert(word_series != NULL);
  assert(word_series_long != NULL);
  assert(*word_series_long > 0);

  // till end of text
  uint16_t text_indexFinger = 0;
  uint16_t word_series_indexFinger = 0;
  uint8_t word_long = 0;
  uint16_t word_begin = 0;
  uint16_t word_number = 0;
  for (word_series_indexFinger = 0;
       word_series_indexFinger < *word_series_long &&
       text_long - text_indexFinger > 1;
       ++word_series_indexFinger) {
    printf("%s:%d:\ttext_indexFinger 0x%X text_long 0x%X text %s\n", __FILE__,
           __LINE__, text_indexFinger, text_long - text_indexFinger,
           text + text_indexFinger);
    first_word_derive((uint8_t)(text_long - text_indexFinger),
                      text + text_indexFinger, &word_long, &word_begin);
    if (word_long == 0) {
      break;
    }
    text_indexFinger += word_begin;
    word_number_encode(word_long, text + text_indexFinger, &word_number);
    word_series[word_series_indexFinger] = word_number;
    text_indexFinger += word_long;
  }
  // get words
  // add to array
  // return  length
  *word_series_long = word_series_indexFinger;
}

#define independentClause_encoding_mood                                        \
  tablet_grammar_write(tablet_indexFinger, number, *tablet_series_long,        \
                       tablet, &tablet_indexFinger);                           \
  current = 2;                                                                 \
  ++tablet_indexFinger;                                                        \
  break;

#define independentClause_encoding_case                                        \
  tablet_grammar_write(tablet_indexFinger, number, *tablet_series_long,        \
                       tablet, &tablet_indexFinger);                           \
  ++tablet_indexFinger;                                                        \
  break;

void independentClause_encoding(const uint16_t text_magnitude, const char *text,
                                uint8_t *tablet_series_long, v16us *tablet,
                                uint16_t *text_remainder) {
  /* algorithm:
      loop through glyphs,
      derive words
      if word is quote then add it to tablet,
          and add proper quote identifier.
      if word is accusative, instrumental, or dative,
          flip the index representation for it.
      if word is deontic-mood then set as ending tablet.
      otherwise add word as name
  */
  char glyph;
  uint8_t text_indexFinger = 0;
  uint16_t tablet_indexFinger = 1;
  uint8_t quote_indexFinger = 0;
  char word[WORD_LONG];
  uint8_t word_magnitude = 0;
  char derived_word[WORD_LONG];
  uint8_t derived_word_magnitude = WORD_LONG;
  uint16_t number = 0;
  uint16_t quote_word = 0;
  // uint16_t binary_phrase_list = (uint16_t)1;
  uint8_t quote_magnitude = 0;
  // uint8_t quote_tablet_series_long = 0;
  uint8_t current = 0x0;
  uint8_t word_long = 0;
  uint16_t word_begin = 0;
  uint16_t quote_sort_code = 0;
  uint16_t quote_sort = 0;
  uint16_t scalar_thick = 0;
  uint16_t name_literal = 0;
  uint16_t word_series[MAXIMUM_WORD_SERIES_LONG] = {0};
  uint16_t word_series_long = MAXIMUM_WORD_SERIES_LONG;
  uint8_t vector_long = 0;
  uint16_t vector_long_word = 0;
  uint16_t series_indexFinger = 0;
  uint16_t word_code = 0;
  uint16_t quote_word_indexFinger = 0;
  uint16_t phrase_long = 0;
  memset(word, 0, WORD_LONG);
  memset(derived_word, 0, WORD_LONG);
  assert(text != NULL);
  assert(text_magnitude > 0);
  assert(tablet != NULL);
  assert(tablet_series_long != NULL);
  assert(text_remainder != NULL);
  uint16_t retrospective_phrase_indexFinger = 0;
  // printf("tablet_series_long %X text %s\n",(uint) *tablet_series_long, text);
  // assert(*tablet_series_long >= text_magnitude/2/15
  // /*MAX_INDEPENDENTCLAUSE_TABLET*/);
  // printf("independentClause encoding\n");
  // tablet_write(0, binary_phrase_list, *tablet_series_long, tablet);
  for (text_indexFinger = 0; text_indexFinger < text_magnitude;
       ++text_indexFinger) {
    glyph = text[text_indexFinger];
    // printf("%s:%d:\t text_magnitude 0x%X text_indexFinger 0x%X text %s\n",
    // __FILE__, __LINE__,
    //      text_magnitude, text_indexFinger, text + text_indexFinger);
    // printf("%s:%d:\t letter %c\n", __FILE__, __LINE__, glyph);
    // printf("%s:%d ", __FILE__, __LINE__);
    // v16us_print(*tablet);
    if (glyph == 0) {
      // text magnitude not detected properly
      assert(1 == 0);
    }
    if (consonant_Q(glyph) == TRUE || vowel_Q(glyph) == TRUE ||
        tone_Q(glyph) == TRUE) {
      word[word_magnitude] = glyph;
      // printf("%c", glyph);
      word_magnitude = (uint8_t)(word_magnitude + 1);
    }
    if (word_magnitude >= 2) {
      // printf("tablet_indexFinger %X\n", (uint) tablet_indexFinger);
      memset(derived_word, 0, WORD_LONG);
      derived_word_magnitude = WORD_LONG;
      derive_first_word(word_magnitude, word, &derived_word_magnitude,
                        derived_word);
      if (derived_word_magnitude > 0) {
        number = 0;
        word_number_encode(derived_word_magnitude, derived_word, &number);
        // printf("n 0x%X \n", (uint) number);
        if (number != 0) {
          memset(word, 0, WORD_LONG);
          switch (number) {
          case number_GRAMMAR:
            /* if preceded by a quote word, then do as a default.
               if preceded by numbers then convert them to a quote,
               and adjust tablet_indexFinger accordingly */
            word_code = tablet_upcoming_word_read(tablet_indexFinger - 1,
                                                  *tablet_series_long, tablet,
                                                  &tablet_indexFinger);
            if ((word_code & QUOTED_DENOTE) == QUOTED_DENOTE) {
              // tablet[0][tablet_indexFinger] = number;
              //++tablet_indexFinger;
            } else if (number_word_INT(word_code) == truth_WORD) {
              /* if last of tablet is a valid number word */ 
              // convert last of tablet to number quote
              // printf("pre tablet_indexFinger %X\n",
              // (uint)tablet_indexFinger);
              printf("%s:%d:%s\n", __FILE__, __LINE__, __FUNCTION__);
              tablet_print(*tablet_series_long, tablet);
              convert_last_number_to_quote(&tablet_indexFinger,
                                           *tablet_series_long, tablet);
              // printf("post tablet_indexFinger %X\n",
              // (uint)tablet_indexFinger);
              // tablet[0][tablet_indexFinger] = number;
              ++tablet_indexFinger;
            } else { // else default 
              quote_word = QUOTE_DENOTE;
              quote_word |= UINT_SORT_DENOTE << SORT_DENOTE_BEGIN;
              quote_word |= NAME_NAME_LITERAL << NAME_LITERAL_BEGIN;
              quote_word |= SIXTEEN_TIDBIT_SCALAR_THICK << SCALAR_THICK_BEGIN;
    printf("%s:%d:%s \n", __FILE__, __LINE__, __FUNCTION__);
              tablet_print(*tablet_series_long, tablet);
              // get previous phrase marker, 
              tablet_retrospective_grammar_read(tablet_indexFinger, 
                  *tablet_series_long, tablet,
                  &retrospective_phrase_indexFinger);
              DEBUGPRINT("0x%X retrospective_phrase_indexFinger,"
                  " 0x%X tablet_indexFinger\n", 
                  retrospective_phrase_indexFinger, tablet_indexFinger);
              // copy all the words forward by one
              phrase_long = tablet_indexFinger - 
                  (retrospective_phrase_indexFinger);
              tablet_upcoming_copy(retrospective_phrase_indexFinger+1, 
                  tablet_indexFinger+1 , phrase_long, *tablet_series_long, tablet);
              // insert quote  word
              tablet_word_write(retrospective_phrase_indexFinger+1, quote_word,
                              *tablet_series_long, tablet,
                              &tablet_indexFinger);
    printf("%s:%d:%s 0x%X phrase_long, 0x%X tablet_indexFinger\n", __FILE__, __LINE__, __FUNCTION__,
        phrase_long, tablet_indexFinger);
              tablet_print(*tablet_series_long, tablet);
              tablet_indexFinger += phrase_long;
              // copy name
              //tablet_print(*tablet_series_long, tablet);
              quote_word = 0;
              ++tablet_indexFinger;
             // ++tablet_indexFinger;
            }
            break;
          case letter_GRAMMAR:
            // find previous end of phrase
            retrospective_phrase_indexFinger_found(
                tablet_indexFinger, *tablet_series_long, tablet,
                &retrospective_phrase_indexFinger);
            printf("%s:%d\tretrospective_indexFinger 0x%X tablet_indexFinger "
                   "0x%X \n,",
                   __FILE__, __LINE__, retrospective_phrase_indexFinger,
                   tablet_indexFinger);
            printf("%s:%d:%s\n\t", __FILE__, __LINE__, __FUNCTION__);
            v16us_print(*tablet);
            // preceding word(s) is/are name
            // copy preceding words forward one
            quote_word_indexFinger = retrospective_phrase_indexFinger + 1;
            tablet_upcoming_copy(quote_word_indexFinger, tablet_indexFinger, 1,
                                 *tablet_series_long, tablet);
            // set up with letter quote denote
            quote_word = QUOTE_DENOTE;
            quote_sort = LETTER_SORT_DENOTE;
            quote_word |= quote_sort << SORT_DENOTE_BEGIN;
            scalar_thick = SIXTEEN_TIDBIT_SCALAR_THICK;
            quote_word |= scalar_thick << SCALAR_THICK_BEGIN;
            name_literal = NAME_NAME_LITERAL;
            quote_word |= name_literal << NAME_LITERAL_BEGIN;
            printf("%s:%d\tquote_word 0x%X\n", __FILE__, __LINE__, quote_word);
            tablet_word_write(quote_word_indexFinger, quote_word,
                              *tablet_series_long, tablet,
                              &quote_word_indexFinger);
            ++tablet_indexFinger;
            printf("%s:%d:%s\n\t", __FILE__, __LINE__, __FUNCTION__);
            v16us_print(*tablet);
            break;
          case vector_GRAMMAR:
            //  // preceding word is length of vector
            //  // find previous quote denote and set length of vector
            retrospective_phrase_indexFinger_found(
                tablet_indexFinger, *tablet_series_long, tablet,
                &retrospective_phrase_indexFinger);
            quote_word_indexFinger = retrospective_phrase_indexFinger + 1;
            quote_word = tablet_upcoming_word_read(quote_word_indexFinger,
                                                   *tablet_series_long, tablet,
                                                   &quote_word_indexFinger);
            assert((quote_word & QUOTE_DENOTE_MASK) == QUOTE_DENOTE);
            --tablet_indexFinger;
            vector_long_word = tablet_upcoming_word_read(
                tablet_indexFinger, *tablet_series_long, tablet,
                &tablet_indexFinger);
            number_word_to_number(vector_long_word, &vector_long);
            assert(vector_long <= MAXIMUM_WORD_NUMBER);
            vector_long = (uint8_t)vector_code(vector_long);
            assert(vector_long <= MAXIMUM_VECTOR_CODE);
            quote_word |= vector_long << VECTOR_THICK_BEGIN;
            tablet_word_write(tablet_indexFinger, 0, *tablet_series_long,
                              tablet, &tablet_indexFinger);
            tablet_word_write(quote_word_indexFinger, quote_word,
                              *tablet_series_long, tablet,
                              &quote_word_indexFinger);
            printf("%s:%d:%s\n\t", __FILE__, __LINE__, __FUNCTION__);
            v16us_print(*tablet);
            break;
          case series_GRAMMAR:
            //  // find previous quote denote, and set to a series
            retrospective_phrase_indexFinger_found(
                tablet_indexFinger, *tablet_series_long, tablet,
                &retrospective_phrase_indexFinger);
            quote_word_indexFinger = retrospective_phrase_indexFinger + 1;
            quote_word = tablet_upcoming_word_read(quote_word_indexFinger,
                                                   *tablet_series_long, tablet,
                                                   &quote_word_indexFinger);
            assert((quote_word & QUOTE_DENOTE_MASK) == QUOTE_DENOTE);
            quote_word |= SERIES_SERIES << SERIES_BEGIN;
            tablet_word_write(tablet_indexFinger, 0, *tablet_series_long,
                              tablet, &tablet_indexFinger);
            tablet_word_write(quote_word_indexFinger, quote_word,
                              *tablet_series_long, tablet,
                              &quote_word_indexFinger);
            printf("%s:%d:%s\n\t", __FILE__, __LINE__, __FUNCTION__);
            v16us_print(*tablet);
            break;
          case text_GRAMMAR:
            printf("%s:%d:%s\t TODO text grammar quote \n", __FILE__, __LINE__,
                   __FUNCTION__);
            //
            break;
          case word_GRAMMAR:
            printf("%s:%d:%s\t TODO word grammar quote \n", __FILE__, __LINE__,
                   __FUNCTION__);
            //  find next word grammar
            // encode text between
            // create applicable type
            // adjust applicable indexFingers
            break;
          case quoted_GRAMMAR:
            // printf("detected quote word %X\n", (uint)text_indexFinger);
            ++text_indexFinger;
            quote_word = 0;
            detect_ACC_quote_magnitude(
                (uint8_t)(text_magnitude - text_indexFinger),
                text + text_indexFinger, &quote_magnitude, &quote_indexFinger);
            printf("%s:%d\tdetected quote size 0x%X indexFinger 0x%X \n",
                   __FILE__, __LINE__, (uint)quote_magnitude,
                   quote_indexFinger);
            // printf("quote_indexFinger %X\n", (uint) quote_indexFinger -
            // text_indexFinger);
            // printf("quote %s\n", text + text_indexFinger +
            // SILENCE_GLYPH_LONG);

            // encode the quote sort word, then switch case it
            //  find quote sort word
            text_indexFinger += SILENCE_GLYPH_LONG;
            printf("%s:%d\tletter %c\n", __FILE__, __LINE__,
                   text[text_indexFinger]);
            first_word_derive(WORD_LONG, text + text_indexFinger, &word_long,
                              &word_begin);
            printf("%s:%d\tletter %c word_long 0x%X\n", __FILE__, __LINE__,
                   text[text_indexFinger + word_begin], word_long);
            word_number_encode(word_long, text + text_indexFinger + word_begin,
                               &quote_sort_code);
            printf("%s:%d\tquote_sort_code %X\n", __FILE__, __LINE__,
                   quote_sort_code);
            switch (quote_sort_code) {
            case word_GRAMMAR:
              printf("%s:%d\tword quote detected\n", __FILE__, __LINE__);
              quote_word = QUOTE_DENOTE;
              quote_sort = WORD_SORT_DENOTE;
              quote_word |= quote_sort << SORT_DENOTE_BEGIN;
              printf("%s:%d\tquote_word 0x%X\n", __FILE__, __LINE__,
                     quote_word);
              scalar_thick = SIXTEEN_TIDBIT_SCALAR_THICK;
              quote_word |= scalar_thick << SCALAR_THICK_BEGIN;
              // find length till next quote_sort_code
              printf("%s:%d\tquote_word 0x%X\n", __FILE__, __LINE__,
                     quote_word);
              // quote_indexFinger and quote_magnitude tell me
              text_indexFinger += quote_indexFinger - SILENCE_GLYPH_LONG;
              // encode them as words
              // convert words into array,
              word_series_encode(quote_magnitude, text + text_indexFinger,
                                 &word_series_long, word_series);
              printf("%s:%d\tword_series_long 0x%X\n", __FILE__, __LINE__,
                     word_series_long);
              // find the best vector size for them
              vector_long = vector_long_find(word_series_long);
              printf("%s:%d\tvector_long 0x%X\n", __FILE__, __LINE__,
                     vector_long);
              // set length appropriately
              printf("%s:%d\tquote_word 0x%X\n", __FILE__, __LINE__,
                     quote_word);
              quote_word |= vector_code(vector_long) << VECTOR_THICK_BEGIN;
              printf("%s:%d\tquote_word 0x%X\n", __FILE__, __LINE__,
                     quote_word);
              // append the quote and words to the tablet
              tablet_word_write(tablet_indexFinger, quote_word,
                                *tablet_series_long, tablet,
                                &tablet_indexFinger);
              ++tablet_indexFinger;
              for (series_indexFinger = 0;
                   series_indexFinger < word_series_long;
                   ++series_indexFinger) {
                tablet_word_write(
                    tablet_indexFinger, word_series[series_indexFinger],
                    *tablet_series_long, tablet, &tablet_indexFinger);
                ++tablet_indexFinger;
              }
              // leave the encoded place and text place properly
              text_indexFinger += quote_magnitude + SILENCE_GLYPH_LONG * 2 +
                                  word_long + QUOTED_WORD_LONG - 1;
              printf("%s:%d\ttext %s\n", __FILE__, __LINE__,
                     text + text_indexFinger);
              break;
            case number_GRAMMAR:
              printf("%s:%d\tnumber quote detected\n", __FILE__, __LINE__);
              break;
            case independent_clause_GRAMMAR:
              printf("%s:%d\tsentence quote detected\n", __FILE__, __LINE__);
              break;
            }
            //          derive_quote_code(
            //              (uint8_t)(quote_indexFinger - SILENCE_GLYPH_LONG),
            //              text + text_indexFinger + SILENCE_GLYPH_LONG,
            //              quote_magnitude,
            //              text + text_indexFinger + quote_indexFinger,
            //              &quote_word);
            // printf("quote_word %X\n", (uint)quote_word);
            //((uint16_t *)tablet)[tablet_indexFinger] = quote_word;
            // v16us_write(tablet_indexFinger, quote_word, tablet);
            //++tablet_indexFinger;
            // copy_ACC_text_DAT_tablet(
            //    text + text_indexFinger + quote_indexFinger, quote_magnitude,
            //    tablet, tablet_indexFinger,
            //    (uint8_t)(*tablet_series_long * TABLET_LONG -
            //              tablet_indexFinger));
            //// printf("text_indexFinger %X, quote_indexFinger %X,
            //// quote_magnitude
            //// %X\n",
            ////       (uint)text_indexFinger, (uint)quote_indexFinger,
            ////       (uint)quote_magnitude);
            // text_indexFinger =
            //    (uint8_t)(text_indexFinger + (quote_indexFinger)*2 +
            //              QUOTED_WORD_LONG + quote_magnitude - 1);
            // word_magnitude = 0;
            // fit_quote_magnitude(quote_magnitude, &quote_tablet_series_long);
            //// printf("qll %X\n", (uint)
            ////     quote_tablet_series_long);
            // tablet_indexFinger =
            //    (uint8_t)(tablet_indexFinger + quote_tablet_series_long);
            ////    printf("ls %X\n", (uint)
            ////        tablet_indexFinger);
            break;

          case nominative_case_GRAMMAR:
            independentClause_encoding_case;
          case accusative_case_GRAMMAR:
            independentClause_encoding_case;
          case instrumental_case_GRAMMAR:
            independentClause_encoding_case;
          case topic_case_GRAMMAR:
            independentClause_encoding_case;
          case dative_case_GRAMMAR:
            independentClause_encoding_case;
          case allative_case_GRAMMAR:
            independentClause_encoding_case;
          case ablative_case_GRAMMAR:
            independentClause_encoding_case;
          case vocative_case_GRAMMAR:
            independentClause_encoding_case;
          case conditional_mood_GRAMMAR:
            tablet_grammar_write(tablet_indexFinger, number,
                                 *tablet_series_long, tablet,
                                 &tablet_indexFinger);
            // binary_phrase_list =
            //    (uint16_t)(binary_phrase_list ^ (1 << tablet_indexFinger));
            ++tablet_indexFinger;
            break;
          case deontic_mood_GRAMMAR:
            independentClause_encoding_mood;
          case epistemic_mood_GRAMMAR:
            independentClause_encoding_mood;
          case realis_mood_GRAMMAR:
            independentClause_encoding_mood;
          case return_GRAMMAR:
            independentClause_encoding_mood;
          case finally_GRAMMAR:
            independentClause_encoding_mood;
          default:
            tablet_word_write(tablet_indexFinger, number, *tablet_series_long,
                              tablet, &tablet_indexFinger);
            ++tablet_indexFinger;
            break;
          }
          if (current == 2)
            break;
          word_magnitude = 0;
        } else {
        }
      }
    }
  }
  ++text_indexFinger;
  //         printf("%s:%d:%s\n", __FILE__, __LINE__,__FUNCTION__);
  //           tablet_print(*tablet_series_long, tablet);
  printf("%s:%d:%s\n\t", __FILE__, __LINE__, __FUNCTION__);
  printf(" text_indexFinger 0x%X, tsl 0x%X\n", text_indexFinger,
         tablet_indexFinger / TABLET_LONG + 1);
  *tablet_series_long = (uint8_t)(tablet_indexFinger - 1) / TABLET_LONG + 1;
  *text_remainder = (uint16_t)(text_magnitude - text_indexFinger);
  // v16us_write(0, binary_phrase_list, tablet);
}

extern inline void establish_ACC_binary_phrase_list(
    const uint16_t *code_text, const uint8_t independentClause_magnitude,
    uint16_t *binary_phrase_list, uint16_t *tablet) {
  uint8_t current = 0;
  uint8_t i = 0;
  assert(code_text != NULL);
  assert(independentClause_magnitude != 0);
  assert(independentClause_magnitude <=
         TABLET_WORD_LONG * MAX_INDEPENDENTCLAUSE_TABLET + 1);
  assert(binary_phrase_list != NULL);
  if (*binary_phrase_list == 0) {
    current = (uint8_t)~current;
  }
  for (i = 0; i < independentClause_magnitude; i++) {
    if (current == 2)
      break;
    switch (current) {
    /*case 0:
        *binary_phrase_list |= 0 << (i + 1);
        break; */
    case 0xFF:
      *binary_phrase_list = (uint16_t)(*binary_phrase_list | (1 << (i + 1)));
      break;
    default:
      break;
    }
    tablet[i + 1] = code_text[i];
    switch (code_text[i]) {
    case accusative_case_GRAMMAR:
      current = (uint8_t)(~current);
      break;
    case instrumental_case_GRAMMAR:
      current = (uint8_t)(~current);
      break;
    case dative_case_GRAMMAR:
      current = (uint8_t)(~current);
      break;
    case allative_case_GRAMMAR:
      current = (uint8_t)(~current);
      break;
    case ablative_case_GRAMMAR:
      current = (uint8_t)(~current);
      break;
    case vocative_case_GRAMMAR:
      current = (uint8_t)(~current);
      break;
    case deontic_mood_GRAMMAR:
      current = 2;
      break;
    case epistemic_mood_GRAMMAR:
      current = 2;
      break;
    case realis_mood_GRAMMAR:
      current = 2;
      break;
    default:
      break;
    }
  }
  tablet[0] = *binary_phrase_list;
}

void tablet_encoding(const uint8_t code_text_magnitude,
                     const uint16_t *code_text, uint8_t *tablet_series_long,
                     uint16_t *tablet, uint8_t *remainder) {
  uint16_t binary_phrase_list = 0;
  uint8_t i = 0;
  uint8_t independentClause_magnitude = 0;
  uint8_t tablet_number = 0;
  assert(code_text != NULL);
  assert(code_text_magnitude > 0);
  assert(tablet != NULL);
  assert(tablet_series_long != NULL);
  assert(remainder != NULL);
  assert(*tablet_series_long >= 16);
  /* algorithm:
      detect end of independentClause marker deontic-mood,
      set remainder as the rest.
      determine if can fit in one tablet or need multiple*/
  for (i = 0; i < code_text_magnitude; i++) {
    if (code_text[i] == deontic_mood_GRAMMAR ||
        code_text[i] == realis_mood_GRAMMAR ||
        code_text[i] == epistemic_mood_GRAMMAR) {
      independentClause_magnitude = (uint8_t)(i + 1);
      break;
    }
  }
  assert(independentClause_magnitude > 0);
  for (i = 0; i < independentClause_magnitude;
       i = (uint8_t)(i + TABLET_WORD_LONG)) {
    if (independentClause_magnitude - i > TABLET_WORD_LONG) {
      binary_phrase_list = 0;
    } else {
      binary_phrase_list = 1;
    }
    establish_ACC_binary_phrase_list(
        code_text + i, (uint8_t)(independentClause_magnitude - i),
        &binary_phrase_list, tablet + tablet_number);
    ++tablet_number;
  }
  *remainder = (uint8_t)(code_text_magnitude - independentClause_magnitude);
  *tablet_series_long = (uint8_t)(tablet_number);
}

void code_ACC_word_PL(const uint8_t ACC_GEN_magnitude,
                      const char *ACC_independentClause,
                      uint8_t *DAT_GEN_magnitude,
                      uint16_t *DAT_code_independentClause,
                      uint8_t *DAT_GEN_remainder) {
  /* identify if two glyph or four glyph word
      if there is a bad parse
      then return an error message,
          which indicates the location of the error.
  assumptions:
      this is can be used in a reduce like manner,
      so many function together, which is enabled if size is
      full width, or 0xFF the last two glyphs can
      overlap with next independentClause, so if they are a four glyph word
      then keep them, otherwise ignore.
  algorithm:
  */
  char DAT_word[WORD_LONG];
  uint8_t DAT_word_GEN_magnitude = WORD_LONG;
  uint8_t i = 0;
  uint8_t j = 0;
  uint16_t number = 0;
  // uint8_t size = ACC_GEN_magnitude;
  // if (ACC_GEN_magnitude == 0xFF) { // see assumptions
  //    size = 0xFF - 2;
  //}
  memset(DAT_word, 0, WORD_LONG);
  assert(ACC_independentClause != NULL);
  assert(ACC_GEN_magnitude > 0);
  assert(DAT_code_independentClause != NULL);
  assert(*DAT_GEN_magnitude >= ACC_GEN_magnitude / 2);
  for (; i < ACC_GEN_magnitude; j++) {
    derive_first_word((uint8_t)(ACC_GEN_magnitude - i),
                      ACC_independentClause + i, &DAT_word_GEN_magnitude,
                      DAT_word);
    // printf("%s %d \n", ACC_independentClause +i,
    //    (int) DAT_word_GEN_magnitude);
    if (DAT_word_GEN_magnitude == 0) {
      *DAT_GEN_remainder = (uint8_t)(ACC_GEN_magnitude - i);
      break;
    }
    word_number_encode(DAT_word_GEN_magnitude, DAT_word, &number);
    i = (uint8_t)(i + DAT_word_GEN_magnitude);
    DAT_code_independentClause[j] = number;
    DAT_word_GEN_magnitude = WORD_LONG;
  }
  *DAT_GEN_magnitude = j;
}
void text_encoding_quiz(const uint16_t max_text_magnitude, const char *text,
                   uint16_t *text_remainder) {
  uint16_t recipe_long = 0xFF;
  v16us recipe[0xFF] = {0};
  text_encoding(max_text_magnitude, text, &recipe_long, recipe, text_remainder);
}
