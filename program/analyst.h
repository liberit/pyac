#ifndef ANALYST_H
#define ANALYST_H
#include "seed.h"
#include "sort.h"
void detect_ACC_quote_magnitude(const uint8_t text_magnitude,
                                              const char *text,
                                              uint8_t *quote_magnitude,
                                              uint8_t *quote_indexFinger);
void derive_first_word(const uint8_t ACC_GEN_magnitude,
                              const char *ACC_independentClause,
                              uint8_t *DAT_GEN_magnitude, char *DAT_word);
#endif
