#ifndef TRANSLATION_H
#define TRANSLATION_H
#include "pyash.h"
#include "sort.h"

#define quote_word_number_modernize                                            \
  gross_text_long += word_long;                                                \
  vacancy_text_long -= word_long;                                              \
  word_long = vacancy_text_long;                                              \
  assert(word_long >= WORD_LONG); \
printf("%s:%d:%s\n", __FILE__, __LINE__, __FUNCTION__);

void word_code_translation(const uint16_t word_code, uint16_t *text_long,
                           char *text);
void tablet_translate(const uint8_t series_long, const v16us *tablet,
                      uint16_t *text_long, char *text);
void code_opencl_translate(const uint16_t recipe_magnitude, const v16us *recipe,
                           uint16_t *produce_text_long, char *text,
                           uint16_t *filename_long, char *filename,
                           uint16_t *file_sort);
void derive_code_name(const uint8_t tablet_magnitude, const v16us *tablet,
                      v4us *code_name);
void code_name_derive(const uint8_t tablet_magnitude, const v16us *tablet,
                      uint64_t *code_name);
uint8_t code_tablet_long_found(const uint16_t recipe_long, const v16us *recipe);
#endif
