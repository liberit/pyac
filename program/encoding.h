#ifndef ENCODING_H
#define ENCODING_H
#include "sort.h"
#include "seed.h"
#include "parser.h"
void word_number_encode(const uint8_t word_size, const char *word,
                        uint16_t *number);
void tablet_encoding(const uint8_t code_text_size, const uint16_t *code_text,
                     uint8_t *tablet_size, uint16_t *tablet,
                     uint8_t *remainder);

void independentClause_encoding(const uint16_t text_size, const char *text,
                                uint8_t *tablet_size, v16us *tablet,
                                uint16_t *remainder);
void code_ACC_word_PL(const uint8_t ACC_GEN_size,
                      const char *ACC_independentClause, uint8_t *DAT_GEN_size,
                      uint16_t *DAT_code_independentClause,
                      uint8_t *DAT_GEN_remainder);

extern inline void code_ACC_consonant_one(const uint8_t type,
                                          const uint8_t consonant_one,
                                          uint16_t *number) ;
extern inline void code_ACC_vowel(const uint8_t type, const uint8_t vowel,
                                  uint16_t *number);
extern inline void code_ACC_tone(const uint8_t type, const uint8_t tone,
                                 uint16_t *number);
extern inline void code_ACC_consonant_three(const uint8_t type,
                                            const uint8_t consonant_three,
                                            const uint8_t tone,
                                            uint16_t *number);
void text_encoding(const uint16_t max_text_magnitude, const char *text,
                   uint16_t *tablet_magnitude, v16us *tablet,
                   uint16_t *text_remainder);
void text_encoding_quiz(const uint16_t max_text_magnitude, const char *text,
                   uint16_t *text_remainder);
#endif

