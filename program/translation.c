#include "translation.h"
#include "parser.h"
#include "pyash.h"
#include "pyashWords.h"
#include "sort.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int phrase_code_compare(const void *a, const void *b) {
  return (int)(*(uint16_t *)a - *(uint16_t *)b);
}
/* implements universal hashing using random bit-vectors in x */
/* assumes number of elements in x is at least BITS_PER_CODE_NAME *
 * MAX_STRING_SIZE
 */
#define BITS_PER_CODE_NAME (64) /* not true on all machines! */
#define MAX_STRING_SIZE (16)    /* we'll stop hashing after this many */
#define MAX_BITS (BITS_PER_CODE_NAME * MAX_STRING_SIZE)
#define SEED_NUMBER UINT64_C(0x123456789ABCDEF)

uint64_t splitMix64(uint64_t *seed) {
  uint64_t z = (*seed += UINT64_C(0x9E3779B97F4A7C15));
  z = (z ^ (z >> 30)) * UINT64_C(0xBF58476D1CE4E5B9);
  z = (z ^ (z >> 27)) * UINT64_C(0x94D049BB133111EB);
  return z ^ (z >> 31);
}
uint64_t hash(const uint8_t array_length, const uint64_t *array) {
  uint64_t hash;
  uint8_t array_indexFinger = 0;
  /*uint8_t bit_indexFinger = 0;*/
  uint64_t random_seed = SEED_NUMBER;
  uint64_t c;
  uint64_t hash_number = 0;
  int shift;

  /* cast s to unsigned const char * */
  /* this ensures that elements of s will be treated as having values >= 0 */

  // printf("array_indexFinger %X\n", array_indexFinger);
  hash = 0;
  for (array_indexFinger = 0; array_indexFinger < array_length;
       ++array_indexFinger) {
    c = array[array_indexFinger];
    for (shift = 0; shift < BITS_PER_CODE_NAME;
         ++shift /*, ++bit_indexFinger*/) {
      /* is low bit of c set? */
      hash_number = splitMix64(&random_seed);
      if (c & 0x1) {
        hash ^= hash_number;
        // printf("hash %lX\n", hash);
      }

      /* shift c to get new bit in lowest position */
      c >>= 1;
    }
  }

  return hash;
}

void consonant_one_code_translation(const uint8_t consonant_one_code,
                                    uint8_t *vocal, char *text) {
  assert(consonant_one_code < CONSONANT_ONE_ENCODE_LONG);
  assert(text != NULL);
  uint8_t indexFinger = 0;
  text[0] = (char)consonant_one_code_group[consonant_one_code][0];
  *vocal = FALSE;
  for (; indexFinger < CONSONANT_ONE_VOCAL_LONG; ++indexFinger) {
    if (consonant_one_code == consonant_one_vocal_group[indexFinger][1]) {
      *vocal = TRUE;
      break;
    }
  }
}

void consonant_two_code_translation(const uint8_t consonant_two_code,
                                    const uint8_t vocal, char *text) {
  assert(consonant_two_code < CONSONANT_TWO_ENCODE_LONG);
  assert(vocal == TRUE || vocal == FALSE);
  assert(text != NULL);
  if (vocal == TRUE) {
    text[0] = (char)consonant_two_vocal_group[consonant_two_code][0];
  } else {
    text[0] = (char)consonant_two_aspirate_group[consonant_two_code][0];
  }
}
void vowel_code_translation(const uint8_t vowel_code, char *text) {
  assert(vowel_code < VOWEL_ENCODE_LONG);
  assert(text != NULL);
  text[0] = (char)vowel_code_group[vowel_code][0];
}
void consonant_three_code_translation(const uint8_t consonant_three_code,
                                      char *text) {
  assert(consonant_three_code < VOWEL_ENCODE_LONG);
  assert(text != NULL);
  text[0] = (char)consonant_three_code_group[consonant_three_code][0];
}
void tone_code_translation(const uint8_t tone_code, uint8_t *tone_letter,
                           char *text) {
  assert(tone_code < TONE_ENCODE_LONG);
  assert(tone_letter != NULL);
  assert(text != NULL);
  char tone = (char)tone_code_group[tone_code][0];
  if (tone == 'M') {
    *tone_letter = FALSE;
  } else {
    text[0] = tone;
    *tone_letter = TRUE;
  }
}

void long_grammar_code_translation(const uint16_t word_code,
                                   uint16_t *text_long, char *text) {
  assert(text_long != NULL);
  assert(text != NULL);
  // set first h
  uint16_t word_long = 0;
  const uint8_t consonant_one_code =
      (word_code & LONG_GRAMMAR_CONSONANT_ONE_MASK) >>
      LONG_GRAMMAR_CONSONANT_ONE_BEGIN;
  assert(consonant_one_code < CONSONANT_ONE_ENCODE_LONG);
  uint8_t vocal = FALSE;
  consonant_one_code_translation(consonant_one_code, &vocal, text + word_long);
  ++word_long;
  const uint8_t consonant_two_code =
      (word_code & LONG_GRAMMAR_CONSONANT_TWO_MASK) >>
      LONG_GRAMMAR_CONSONANT_TWO_BEGIN;
  consonant_two_code_translation(consonant_two_code, vocal, text + word_long);
  ++word_long;
  const uint8_t vowel_code =
      (word_code & LONG_GRAMMAR_VOWEL_MASK) >> LONG_GRAMMAR_VOWEL_BEGIN;
  vowel_code_translation(vowel_code, text + word_long);
  ++word_long;
  const uint8_t tone_code =
      (word_code & LONG_GRAMMAR_TONE_MASK) >> LONG_GRAMMAR_TONE_BEGIN;
  uint8_t tone_letter = FALSE;
  tone_code_translation(tone_code, &tone_letter, text + word_long);
  if (tone_letter == TRUE) {
    ++word_long;
  }
  text[word_long] = 'h';
  ++word_long;
  *text_long = word_long;
}
void short_grammar_code_translation(const uint16_t word_code,
                                    uint16_t *text_long, char *text) {
  assert(text_long != NULL);
  assert(text != NULL);
  // set first h
  uint16_t word_long = 0;
  const uint8_t consonant_one_code =
      (word_code & SHORT_GRAMMAR_CONSONANT_ONE_MASK) >>
      SHORT_GRAMMAR_CONSONANT_ONE_BEGIN;
  assert(consonant_one_code < CONSONANT_ONE_ENCODE_LONG);
  uint8_t vocal = FALSE;
  consonant_one_code_translation(consonant_one_code, &vocal, text + word_long);
  ++word_long;
  const uint8_t vowel_code =
      (word_code & SHORT_GRAMMAR_VOWEL_MASK) >> SHORT_GRAMMAR_VOWEL_BEGIN;
  vowel_code_translation(vowel_code, text + word_long);
  ++word_long;
  const uint8_t tone_code =
      (word_code & SHORT_GRAMMAR_TONE_MASK) >> SHORT_GRAMMAR_TONE_BEGIN;
  uint8_t tone_letter = FALSE;
  tone_code_translation(tone_code, &tone_letter, text + word_long);
  if (tone_letter == TRUE) {
    ++word_long;
  }
  *text_long = word_long;
}
void long_root_code_translation(const uint16_t word_code, uint16_t *text_long,
                                char *text) {
  assert(text_long != NULL);
  assert(text != NULL);
  // set first h
  uint8_t word_long = 0;
  const uint8_t consonant_one_code =
      (word_code & LONG_ROOT_CONSONANT_ONE_MASK) >>
      LONG_ROOT_CONSONANT_ONE_BEGIN;
  uint8_t vocal = FALSE;
  consonant_one_code_translation(consonant_one_code, &vocal, text);
  ++word_long;
  const uint8_t consonant_two_code =
      (word_code & LONG_ROOT_CONSONANT_TWO_MASK) >>
      LONG_ROOT_CONSONANT_TWO_BEGIN;
  consonant_two_code_translation(consonant_two_code, vocal, text + word_long);
  ++word_long;
  // printf("consonant_two_code %X, text %s\n", consonant_two_code, text);
  const uint8_t vowel_code =
      (word_code & LONG_ROOT_VOWEL_MASK) >> LONG_ROOT_VOWEL_BEGIN;
  vowel_code_translation(vowel_code, text + word_long);
  ++word_long;
  const uint8_t tone_code =
      (word_code & LONG_ROOT_TONE_MASK) >> LONG_ROOT_TONE_BEGIN;
  uint8_t tone_letter = FALSE;
  tone_code_translation(tone_code, &tone_letter, text + word_long);
  if (tone_letter == TRUE) {
    ++word_long;
  }
  const uint8_t consonant_three_code =
      (word_code & LONG_ROOT_CONSONANT_THREE_MASK) >>
      LONG_ROOT_CONSONANT_THREE_BEGIN;
  consonant_three_code_translation(consonant_three_code, text + word_long);
  ++word_long;
  *text_long = word_long;
}
void phrase_quote_copy(const uint16_t phrase_termination_indexFinger,
                       const uint8_t tablet_long, const v16us *tablet,
                       uint64_t *phrase) {
  // copies the remainder of phrase from back,
  //  stops if indicator says start of a phrase
  assert(phrase != NULL);
  // printf("%s:%d:%s 0x%X pti\n", __FILE__, __LINE__, __FUNCTION__,
  // phrase_termination_indexFinger);
  assert(phrase_termination_indexFinger > 0 &&
         phrase_termination_indexFinger < TABLET_LONG * tablet_long);
  //  const uint16_t indicator_list =
  //  (uint16_t)tablet[phrase_termination_indexFinger/TABLET_LONG].s0;
  //  const uint8_t indicator = 1 & indicator_list;
  //  uint16_t code_word = 0;
  uint16_t indexFinger = phrase_termination_indexFinger - 1;
  uint8_t phrase_number_indexFinger = 0;
  uint16_t retrospective_phrase_indexFinger = 0;
  // uint64_t temporary_phrase = 0;
  tablet_retrospective_grammar_read(indexFinger, tablet_long, tablet,
                                    &retrospective_phrase_indexFinger);
  *phrase = 0;
  *phrase = (uint64_t)tablet_retrospective_word_read(
      phrase_termination_indexFinger, tablet_long, tablet, &indexFinger);
  // printf("%s:%d:%s 0x%lX phrase\n", __FILE__, __LINE__, __FUNCTION__,
  // *phrase);
  ++phrase_number_indexFinger;
  uint16_t quote_word_indexFinger = 0;
  uint16_t quote_word =
      tablet_upcoming_word_read(retrospective_phrase_indexFinger + 1,
                                tablet_long, tablet, &quote_word_indexFinger);
  // printf("%s:%d:%s 0x%X quote_word\n", __FILE__, __LINE__, __FUNCTION__,
  // quote_word);
  if ((quote_word & QUOTE_DENOTE_MASK) == QUOTE_DENOTE) {
    //   temporary_phrase = (
    //       ((uint64_t) quote_word) << (phrase_number_indexFinger *
    //       CODE_WORD_TIDBIT_LONG));
    // printf("%s:%d:%s 0x%lX tphrase\n", __FILE__, __LINE__, __FUNCTION__,
    // temporary_phrase);
    *phrase |= (((uint64_t)quote_word)
                << (phrase_number_indexFinger * CODE_WORD_TIDBIT_LONG));
  }
}

void verb_copy(const uint16_t phrase_termination_indexFinger,
               const uint8_t tablet_long, const v16us *tablet,
               uint64_t *phrase) {
  // copies the remainder of phrase from back,
  //  stops if indicator says start of a phrase
  assert(phrase != NULL);
  assert(phrase_termination_indexFinger > 0 &&
         phrase_termination_indexFinger < TABLET_LONG * tablet_long);
  // const uint16_t indicator_list =
  //    (uint16_t)tablet[phrase_termination_indexFinger / TABLET_LONG].s0;
  // const uint8_t indicator = 1 & indicator_list;
  uint16_t indexFinger = phrase_termination_indexFinger - 1;
  uint16_t retrospective_phrase_indexFinger = 0;
  uint16_t sort_word_indexFinger = 0;
  tablet_retrospective_grammar_read(phrase_termination_indexFinger - 1,
                                    tablet_long, tablet,
                                    &retrospective_phrase_indexFinger);

  // printf("%s:%d:%s\n\t 0x%X pti, 0x%X rpi\n", __FILE__, __LINE__,
  // __FUNCTION__,
  //    phrase_termination_indexFinger, retrospective_phrase_indexFinger);
  *phrase = 0;
  for (; indexFinger > retrospective_phrase_indexFinger; --indexFinger) {
    // if (((indicator_list >> indexFinger) & 1) == indicator) {
    //  break;
    //}
    assert(sort_word_indexFinger < 4);
    *phrase |= ((uint64_t)tablet_retrospective_word_read(
                   indexFinger, tablet_long, tablet, &indexFinger))
               << (CODE_WORD_TIDBIT_LONG * sort_word_indexFinger);
    ++sort_word_indexFinger;
  }
}

void short_root_code_translation(const uint16_t word_code, uint16_t *text_long,
                                 char *text) {
  assert(text_long != NULL);
  assert(text != NULL);
  // set first h
  uint16_t word_long = 0;
  text[0] = 'h';
  ++word_long;
  const uint8_t consonant_one_code =
      (word_code & SHORT_ROOT_CONSONANT_ONE_MASK) >>
      SHORT_ROOT_CONSONANT_ONE_BEGIN;
  assert(consonant_one_code < CONSONANT_ONE_ENCODE_LONG);
  uint8_t vocal = FALSE;
  consonant_one_code_translation(consonant_one_code, &vocal, text + word_long);
  ++word_long;
  const uint8_t vowel_code =
      (word_code & SHORT_ROOT_VOWEL_MASK) >> SHORT_ROOT_VOWEL_BEGIN;
  vowel_code_translation(vowel_code, text + word_long);
  ++word_long;
  const uint8_t tone_code =
      (word_code & SHORT_ROOT_TONE_MASK) >> SHORT_ROOT_TONE_BEGIN;
  uint8_t tone_letter = FALSE;
  tone_code_translation(tone_code, &tone_letter, text + word_long);
  if (tone_letter == TRUE) {
    ++word_long;
  }
  const uint8_t consonant_three_code =
      (word_code & SHORT_ROOT_CONSONANT_THREE_MASK) >>
      SHORT_ROOT_CONSONANT_THREE_BEGIN;
  consonant_three_code_translation(consonant_three_code, text + word_long);
  ++word_long;
  *text_long = word_long;
}

void quote_word_translate(uint16_t quote_code, uint16_t *text_long,
                          char *text);
void word_code_translate(const uint16_t word_code, uint16_t *text_long,
                           char *text) {
  assert(text != NULL);
  assert(text_long != NULL);
  assert(*text_long >= WORD_LONG);
  const uint16_t short_sort = word_code & SHORT_SORT_MASK;
  const uint16_t long_sort = word_code & LONG_ROOT_CONSONANT_ONE_MASK;
  //printf("%s:%d 0x%04X word_code, 0x%X long_sort, 0x%X short_sort \n", __FILE__,
  //    __LINE__, word_code, long_sort, short_sort);
  if (word_code == 0) {
    *text_long = 0;
  } else if (short_sort == SHORT_ROOT_DENOTE) {
    short_root_code_translation(word_code, text_long, text);
  } else if (short_sort == LONG_GRAMMAR_DENOTE) {
    long_grammar_code_translation(word_code, text_long, text);
  } else if (long_sort == SHORT_GRAMMAR_DENOTE) {
    short_grammar_code_translation(word_code, text_long, text);
  } else if (long_sort == QUOTE_DENOTE) {
    quote_word_translate(word_code, text_long, text);
  } else if (word_code > 0x800) {
    long_root_code_translation(word_code, text_long, text);
  } else {
    *text_long = 0;
  }
}

void phrase_situate(const uint8_t tablet_long, const v16us *tablet,
                    const uint16_t phrase_code, uint16_t *phrase_place,
                    uint8_t *phrase_long) {
  // phrase_place is begining of phrase,
  // if not found, returns 0 as phrase_place
  assert(tablet[0].s0 != 0);
  assert(phrase_place != NULL);
  assert(phrase_long != NULL);
  uint16_t tablet_indexFinger = 0;
  printf("grammaticalCase_word %04X \n", phrase_code);
  // (uint16_t)tablet[tablet_indexFinger].s0;
  // const uint16_t referential = (uint8_t)binary_phrase_list & 1;
  // uint8_t indexFinger = 0;
  // uint8_t found = FALSE;
  uint16_t phrase_termination = 0;
  uint16_t phrase_begin = 0;
  uint16_t phrase_word = 0;
  for (; tablet_indexFinger < tablet_long * TABLET_LONG; ++tablet_indexFinger) {
    phrase_word = tablet_upcoming_grammar_read(tablet_indexFinger, tablet_long,
                                               tablet, &tablet_indexFinger);
    if (phrase_word == 0) {
      *phrase_place = 0;
      *phrase_long = 0;
      break;
    }
    printf("%s:%d:%s\t 0x%X tablet_indexFinger, 0x%X phrase_word, "
        "0x%X phrase_code\n", __FILE__,
     __LINE__, __FUNCTION__, tablet_indexFinger, phrase_word, phrase_code);
    if (phrase_word == phrase_code) {
      phrase_termination = tablet_indexFinger;
      --tablet_indexFinger;
      tablet_retrospective_grammar_read(tablet_indexFinger, tablet_long, tablet,
                                        &tablet_indexFinger);
      phrase_begin = tablet_indexFinger + 1;
    printf("%s:%d:%s\t 0x%X tablet_indexFinger\n", __FILE__,
     __LINE__, __FUNCTION__, tablet_indexFinger);
      break;
    }
  }
  *phrase_place = phrase_begin;
  *phrase_long = (uint8_t)(phrase_termination + 1 - phrase_begin);
}
void language_include_establish(uint16_t *produce_text_long,
                                char *produce_text) {
  assert(produce_text != NULL);
  assert(produce_text_long != NULL);
  const char *recipe_text = "#include \"pyac.h\"\n";
  const uint16_t recipe_text_magnitude = (uint16_t)strlen(recipe_text);
  // printf("%s:%d recipe_text_magnitude 0x%X\n", __FILE__, __LINE__,
  //       recipe_text_magnitude);
  memcpy(produce_text, recipe_text, recipe_text_magnitude);
  *produce_text_long = recipe_text_magnitude;
}

void cardinal_translate(uint16_t *produce_text_long, char *produce_text,
                        uint16_t *file_sort) {
  assert(produce_text != NULL);
  assert(produce_text_long != NULL);
  assert(file_sort != NULL);
  const char *recipe_text = "int main() {\n"
                            "setlocale(LC_ALL,\"C\");\n"
                            "bindtextdomain(\"pyac\",\".\");\n"
                            "textdomain(\"pyac\");\n";
  const uint16_t recipe_text_magnitude = (uint16_t)strlen(recipe_text);
  // printf("%s:%d recipe_text_magnitude 0x%X\n", __FILE__, __LINE__,
  //       recipe_text_magnitude);
  memcpy(produce_text, recipe_text, recipe_text_magnitude);
  *produce_text_long = recipe_text_magnitude;
  *file_sort = cardinal_WORD;
}

void return_translate(const uint8_t recipe_long, const v16us *recipe,
                      uint16_t *produce_text_long, char *produce_text) {
  assert(produce_text != NULL);
  assert(produce_text[0] == (char)0); // must be clean
  // get number quote from recipe
  uint16_t phrase_place = 0xF;
  uint8_t phrase_long = 0;
  phrase_situate(recipe_long, recipe, return_GRAMMAR, &phrase_place,
                 &phrase_long);
  uint16_t number_place = phrase_place + phrase_long - 2;
  const int return_number = (int)tablet_retrospective_word_read(
      number_place, recipe_long, recipe, &number_place);
  sprintf(produce_text, "return 0x%X;\n", return_number);
  *produce_text_long = (uint16_t)strlen(produce_text);
}
void natural_number_translate(const uint8_t recipe_long, const v16us *recipe,
                              uint16_t *produce_text_long, char *produce_text) {
  assert(produce_text != NULL);
  assert(produce_text[0] == (char)0); // must be clean
                                      // get number quote from recipe
  // printf("%s:%d %s \n", __FILE__, __LINE__, __FUNCTION__);
  uint16_t phrase_place = 0xF;
  uint8_t phrase_long = 0;
  phrase_situate(recipe_long, recipe, accusative_case_GRAMMAR, &phrase_place,
                 &phrase_long);
  uint16_t number_place = phrase_place + 1;
  printf("%s:%d number_place %x \n", __FILE__, __LINE__, number_place);
  const int return_number = (int)tablet_retrospective_word_read(
      number_place, recipe_long, recipe, &number_place);
  uint16_t name_indexFinger = 0;
  char name[WORD_LONG * 8] = {0};
  const uint16_t name_long_maximum = WORD_LONG * 8;
  uint16_t name_remains = name_long_maximum;
  uint16_t name_long = 0;
  phrase_place = 0xF;
  //tablet_print(recipe_long, recipe);
  phrase_situate(recipe_long, recipe, nominative_case_GRAMMAR, &phrase_place,
                 &phrase_long);
  // printf("%s:%d phrase_place 0x%X \n", __FILE__, __LINE__, phrase_place);
  for (name_indexFinger = phrase_place;
       name_indexFinger < phrase_place + phrase_long - 1; ++name_indexFinger) {
    word_code_translate(tablet_upcoming_word_read(name_indexFinger,
                                                    recipe_long, recipe,
                                                    &name_indexFinger),
                          &name_remains, name + name_long);
    printf("%s:%d name %s name_remains 0x%X \n", __FILE__, __LINE__, name,
           name_remains);
    name_long += name_remains;
    name_remains = name_long_maximum - name_long;
  }
  sprintf(produce_text, "uint16_t %s = 0x%X;\n", name, return_number);
  *produce_text_long = (uint16_t)strlen(produce_text);
}
void name_extract(const uint8_t recipe_long, const v16us *recipe,
                  const uint16_t phrase, uint16_t *name_text_long,
                  char *name_text) {
  uint16_t phrase_place = 0xF;
  uint8_t phrase_long = 0;
  const uint16_t name_long_maximum = WORD_LONG * 8;
  uint16_t name_indexFinger = 0;
  uint16_t name_remains = name_long_maximum;
  uint16_t name_long = 0;
  phrase_situate(recipe_long, recipe, phrase, &phrase_place, &phrase_long);
  uint16_t word = 0;
  printf("%s:%d:%s 0x%X phrase_place, 0x%X phrase_long\n", __FILE__, __LINE__,
         __FUNCTION__, phrase_place, phrase_long);
  for (name_indexFinger = phrase_place;
       name_indexFinger < phrase_long + phrase_place - 1; ++name_indexFinger) {
    assert(name_remains >= WORD_LONG);
    word = tablet_upcoming_word_read(name_indexFinger, recipe_long, recipe,
                                     &name_indexFinger);
    printf("%s:%d:%s 0x%X indexFinger, 0x%X max_indexFinger, 0x%X word\n",
           __FILE__, __LINE__, __FUNCTION__, name_indexFinger,
           phrase_long + phrase_place, word);
    word_code_translate(word, &name_remains, name_text + name_long);
    printf("%s:%d:%s  %s name, 0x%X remains\n", __FILE__, __LINE__,
           __FUNCTION__, name_text, name_remains);
    name_long += name_remains;
    name_remains = name_long_maximum - name_long;
  }
  *name_text_long = name_long;
}

void filename_establish(const uint8_t recipe_long, const v16us *recipe,
                        uint16_t *produce_filename_long, char *filename) {
  assert(filename != NULL);
  assert(produce_filename_long != NULL);
  name_extract(recipe_long, recipe, nominative_case_GRAMMAR,
               produce_filename_long, filename);
}

void sort_array_sort(const uint8_t array_long, uint64_t *sort_array) {
  assert(sort_array != NULL);
  qsort(sort_array, array_long - 1 /* last one is verb, don't sort it!*/,
        sizeof(uint64_t), phrase_code_compare);
}
void sort_array_establish(const uint8_t tablet_long, const v16us *tablet,
                          uint8_t *sort_array_long, uint64_t *sort_array) {
  assert(sort_array_long != NULL);
  assert(sort_array != NULL);
  assert(tablet != NULL);
  assert(tablet_long > 0);
  // algorithm
  // include cases, types, topic, and verb
  // include
  uint16_t tablet_indexFinger = 1;
  uint16_t indicator_list = 0;
  uint8_t indicator = 0;
  uint8_t exit = FALSE;
  uint16_t word = 0;
  uint8_t sort_array_indexFinger = 0;
  uint16_t quote_sort = 0;
  uint64_t phrase = 0;
  uint16_t maximum_tablet_long = tablet_long * TABLET_LONG;
  // uint8_t code_indexFinger = 0;
  // v8us quote_fill = {{0}};
  indicator_list = v16us_read(0, *tablet);
  indicator = (uint8_t)1 & indicator_list;
  // uint8_t indexFinger = 0;
  // printf("indicator %X\n", (uint) indicator);
  // printf("indicator_list %X\n", (uint)indicator_list);
  // printf("%s:%d:%s\n\t 0x%X word, 0x%X indexFinger 0x%lX phrase\n", __FILE__,
  // __LINE__,
  //   __FUNCTION__, word, tablet_indexFinger, phrase);
  for (tablet_indexFinger = 1; tablet_indexFinger < maximum_tablet_long;
       ++tablet_indexFinger) {
    // sort_array[sort_array_indexFinger] = 0;
    phrase = 0;
    //printf("%s:%d:%s\n\t 0x%X word, 0x%X indexFinger 0x%lX phrase\n", __FILE__,
    //       __LINE__, __FUNCTION__, word, tablet_indexFinger, (ulong)phrase);
    word = tablet_upcoming_grammar_or_quote_read(
        tablet_indexFinger, tablet_long, tablet, &tablet_indexFinger);
    // printf("%s:%d:%s\n\t 0x%X tablet_indexFinger\n", __FILE__, __LINE__,
    // __FUNCTION__, tablet_indexFinger);
    if (tablet_indexFinger > maximum_tablet_long) {
      printf("%s:%d:%s\n\t breaking\n", __FILE__, __LINE__, __FUNCTION__);
      break;
    }
    if ((word & QUOTE_DENOTE_MASK) == QUOTED_DENOTE) {
      quote_sort = word;
      continue;
    }
    // printf("%s:%d:%s\n\t 0x%X word, 0x%X indexFinger, 0x%lX phrase\n",
    // __FILE__, __LINE__,
    //    __FUNCTION__, word, tablet_indexFinger, phrase);
    // printf("word %04X SAI%X\n", word, sort_array_indexFinger);
    switch (word) {
    case topic_case_GRAMMAR:
      verb_copy(tablet_indexFinger, tablet_long, tablet, &phrase);
      phrase = (phrase << CODE_WORD_TIDBIT_LONG) | topic_case_GRAMMAR;
      sort_array[sort_array_indexFinger] |= phrase;
      // printf("topic case set %016lX\n",
      // sort_array[sort_array_indexFinger]);
      ++sort_array_indexFinger;
      break;
    case return_GRAMMAR:
      word = tablet_read(tablet_indexFinger, tablet_long, tablet);
      sort_array[sort_array_indexFinger] = (uint64_t)0;
      sort_array[sort_array_indexFinger] = word;
      sort_array[sort_array_indexFinger] |= ((uint64_t)quote_sort)
                                            << CODE_WORD_TIDBIT_LONG;
      // printf("return detected %016lX\n",
      //       sort_array[sort_array_indexFinger]);
      exit = TRUE;
      break;
    case conditional_mood_GRAMMAR:
      verb_copy(tablet_indexFinger, tablet_long, tablet, &phrase);
      sort_array[sort_array_indexFinger] |= phrase;
      exit = TRUE;
      break;
    case deontic_mood_GRAMMAR:
      verb_copy(tablet_indexFinger, tablet_long, tablet, &phrase);
      sort_array[sort_array_indexFinger] |= phrase;
      exit = TRUE;
      break;
    case epistemic_mood_GRAMMAR:
      verb_copy(tablet_indexFinger, tablet_long, tablet, &phrase);
      sort_array[sort_array_indexFinger] |= phrase;
      exit = TRUE;
      break;
    case realis_mood_GRAMMAR:
      verb_copy(tablet_indexFinger, tablet_long, tablet, &phrase);
      sort_array[sort_array_indexFinger] |= phrase;
      exit = TRUE;
      break;
    case finally_GRAMMAR:
      phrase_quote_copy(tablet_indexFinger, tablet_long, tablet, &phrase);
      sort_array[sort_array_indexFinger] |= phrase;
      exit = TRUE;
      break;
    default:
      sort_array[sort_array_indexFinger] = (uint64_t)0;
      // printf("%s:%d\n\t 0x%X quote_sort, 0x%lX phrase\n", __FILE__, __LINE__,
      // quote_sort, phrase);
      if (quote_sort != 0) {
        sort_array[sort_array_indexFinger] = word;
        sort_array[sort_array_indexFinger] |=
            ((uint64_t)quote_sort << CODE_WORD_TIDBIT_LONG);
        // attach the grammar words if no quote sort
        quote_sort = 0;
      } else {
        phrase_quote_copy(tablet_indexFinger, tablet_long, tablet, &phrase);
        sort_array[sort_array_indexFinger] |= phrase;
      }
      //printf("%s:%d:%s\n\t 0x%X quote_sort, 0x%lX phrase\n", __FILE__, __LINE__,
      //       __FUNCTION__, quote_sort, (ulong)phrase);
      // printf("case detected %016lX\n",
      // sort_array[sort_array_indexFinger]);
      ++sort_array_indexFinger;
      break;
    }

    if (exit == TRUE) {
      // load verb
      // printf("verb set %016lX\n", sort_array[sort_array_indexFinger]);
      ++sort_array_indexFinger;
      break;
    }
  }
  // printf("exit %X\n", exit);
  sort_array_sort(sort_array_indexFinger, sort_array);
  *sort_array_long = sort_array_indexFinger;
}

uint16_t vector_long_translate(uint16_t vector_code) {
  switch (vector_code) {
  case VECTOR_THICK_1:
    return 1;
  case VECTOR_THICK_2:
    return 2;
  case VECTOR_THICK_3:
    return 3;
  case VECTOR_THICK_4:
    return 4;
  case VECTOR_THICK_8:
    return 8;
  case VECTOR_THICK_16:
    return 16;
  default:
    assert(1 == 0);
  }
}
void slogan_argument_translate(const uint16_t previous_indicator, const uint16_t tablet_indexFinger,
                        const uint8_t tablet_long, const v16us* tablet, uint16_t *text_long, char *text) {
  assert(text != NULL);
  assert(text_long != NULL);
  assert(*text_long >= 0);
  assert(tablet != NULL);
  assert(tablet_long > 0);
  assert(tablet_indexFinger > previous_indicator);
  // get type
  uint16_t indexFinger = 0;
  uint16_t quote_sort = tablet_upcoming_word_read((uint16_t) previous_indicator + 1, 
      tablet_long, tablet, &indexFinger);

    printf("%s:%d:%s 0x%X quote_sort \n", __FILE__, __LINE__, __FUNCTION__, quote_sort);
  uint16_t sort_denote = (quote_sort & SORT_DENOTE_MASK) >> SORT_DENOTE_BEGIN;
  uint16_t vector_long = vector_long_translate(
      (quote_sort & VECTOR_THICK_MASK) >> VECTOR_THICK_BEGIN);
  uint16_t word = 0;
  uint16_t gross_text_long = 0;
  uint16_t vacant_text_long = *text_long;
  uint16_t word_long;
  uint16_t sprintf_long = 0;
  // act appropriately
  switch (sort_denote) {
  // if text then return string quote of the length of the vector
  // if word then translate each to ASCII, and make string quote
  case WORD_SORT_DENOTE:
    // get length
    text[gross_text_long] = '_';
    ++gross_text_long;
    --vacant_text_long;
    text[gross_text_long] = '(';
    ++gross_text_long;
    --vacant_text_long;
    text[gross_text_long] = '"';
    ++gross_text_long;
    --vacant_text_long;
    for (indexFinger = previous_indicator + 2; indexFinger < vector_long; ++indexFinger) {
      word = tablet_upcoming_word_read(indexFinger, tablet_long, tablet, &indexFinger);
      word_long = vacant_text_long;
      word_code_translate(word, &word_long, text + gross_text_long);
      gross_text_long += word_long;
      vacant_text_long -= word_long;
    }
    text[gross_text_long] = '"';
    ++gross_text_long;
    --vacant_text_long;
    text[gross_text_long] = ')';
    ++gross_text_long;
    --vacant_text_long;
    *text_long = gross_text_long;
    break;
  // if number then convert to hexadecimal number
  case UINT_SORT_DENOTE:
    sprintf_long += sprintf(text + gross_text_long, "uint16_t ");
    gross_text_long += sprintf_long;
    vacant_text_long -= sprintf_long;
    tablet_print(tablet_long, tablet);
    for (indexFinger = previous_indicator +2; indexFinger < tablet_indexFinger; ++indexFinger) {
      word = tablet_upcoming_word_read(indexFinger, tablet_long, tablet, &indexFinger);
    printf("%s:%d:%s 0x%X previous_indicator, 0x%X indexFinger, 0x%X word \n", __FILE__, __LINE__, __FUNCTION__, previous_indicator, indexFinger, word);
      word_long = vacant_text_long;
      word_code_translate(word, &word_long, text + gross_text_long);
      gross_text_long += word_long;
      vacant_text_long -= word_long;
    printf("%s:%d:%s 0x%X previous_indicator, 0x%X indexFinger, 0x%X word \n", __FILE__, __LINE__, __FUNCTION__, previous_indicator, indexFinger, word);
    }
    word = tablet_upcoming_word_read(previous_indicator+1, tablet_long, tablet, &indexFinger);
    word_long = vacant_text_long;
    word_code_translate(word, &word_long, text + gross_text_long);
    gross_text_long += word_long;
    vacant_text_long -= word_long;
    DEBUGPRINT("%s text\n", text);
    break;
  case INT_SORT_DENOTE:
    sprintf_long += sprintf(text + gross_text_long, "int16_t ");
    gross_text_long += sprintf_long;
    vacant_text_long -= sprintf_long;
    for (indexFinger = previous_indicator; indexFinger < tablet_indexFinger; ++indexFinger) {
      word = tablet_upcoming_word_read(indexFinger, tablet_long, tablet, &indexFinger);
      word_long = vacant_text_long;
      word_code_translate(word, &word_long, text + gross_text_long);
      gross_text_long += word_long;
      vacant_text_long -= word_long;
    }
    break;
  case INDEPENDENT_CLAUSE_SORT_DENOTE:
    sprintf_long += sprintf(text + gross_text_long, "v16us ");
    gross_text_long += sprintf_long;
    vacant_text_long -= sprintf_long;
    for (indexFinger = previous_indicator; indexFinger < tablet_indexFinger; ++indexFinger) {
      word = tablet_upcoming_word_read(indexFinger, tablet_long, tablet, &indexFinger);
      word_long = vacant_text_long;
      word_code_translate(word, &word_long, text + gross_text_long);
      gross_text_long += word_long;
      vacant_text_long -= word_long;
    }
    break;
  default:
      printf("%s:%d:%s\t 0x%X sort_denote\n", __FILE__, __LINE__,
             __FUNCTION__, sort_denote);
      tablet_print(tablet_long, tablet);
    assert(1 == 0);
  }
  *text_long = gross_text_long;
}
void summon_argument_translate(uint8_t previous_indicator, uint8_t tablet_indexFinger,
                        v16us tablet, uint16_t *text_long, char *text) {
  assert(tablet_indexFinger > previous_indicator);
  // get type
  uint16_t quote_sort = v16us_read(previous_indicator + 1, tablet);
  uint16_t sort_denote = (quote_sort & SORT_DENOTE_MASK) >> SORT_DENOTE_BEGIN;
  uint16_t vector_long = vector_long_translate(
      (quote_sort & VECTOR_THICK_MASK) >> VECTOR_THICK_BEGIN);
  uint8_t indexFinger = 0;
  uint16_t word = 0;
  uint16_t gross_text_long = 0;
  uint16_t vacant_text_long = *text_long;
  uint16_t word_long;
  // act appropriately
  switch (sort_denote) {
  // if text then return string quote of the length of the vector
  // if word then translate each to ASCII, and make string quote
  case WORD_SORT_DENOTE:
    // get length
    text[gross_text_long] = '_';
    ++gross_text_long;
    --vacant_text_long;
    text[gross_text_long] = '(';
    ++gross_text_long;
    --vacant_text_long;
    text[gross_text_long] = '"';
    ++gross_text_long;
    --vacant_text_long;
    for (indexFinger = 0; indexFinger < vector_long; ++indexFinger) {
      word = v16us_read(indexFinger + previous_indicator + 2, tablet);
      word_long = vacant_text_long;
      word_code_translate(word, &word_long, text + gross_text_long);
      gross_text_long += word_long;
      vacant_text_long -= word_long;
    }
    text[gross_text_long] = '"';
    ++gross_text_long;
    --vacant_text_long;
    text[gross_text_long] = ')';
    ++gross_text_long;
    --vacant_text_long;
    *text_long = gross_text_long;
    break;
  // if number then convert to hexadecimal number
  default:
      printf("%s:%d:%s\n\t 0x%X sort_denote\n", __FILE__, __LINE__,
             __FUNCTION__, sort_denote);
    assert(1 == 0);
  }
}
void slogan_input_translate(const uint8_t tablet_long, const v16us *tablet,
                              uint16_t *text_long, char *text) {
  assert(tablet != NULL);
  assert(text_long != NULL);
  assert(text != NULL);
  // for each grammatical case return the literal C values
  uint16_t tablet_indexFinger = 1;
  uint16_t word_code = 0;
  uint16_t previous_indicator = 0;
  uint8_t finally = FALSE;
  uint16_t max_tablet_long = tablet_long * TABLET_LONG;
  uint16_t phrase_indicator = 0;
  uint8_t input_number = 0;
  uint16_t input_long = 0;
  uint16_t argument_long = *text_long;
  for (tablet_indexFinger = 1; tablet_indexFinger < max_tablet_long; 
      ++tablet_indexFinger) {
    tablet_upcoming_grammar_read(tablet_indexFinger, tablet_long, tablet, 
        &tablet_indexFinger);
    phrase_indicator = tablet_indexFinger;
    word_code = tablet_upcoming_word_read(tablet_indexFinger, tablet_long, 
          tablet, &tablet_indexFinger);
    printf("%s:%d:%s 0x%X previous_indicator, 0x%X tablet_indexFinger, 0x%X word \n", __FILE__, __LINE__, __FUNCTION__, previous_indicator, tablet_indexFinger, word_code);
    switch (word_code) {
    case topic_case_GRAMMAR:
      break;
    case nominative_case_GRAMMAR:
      break;
    case epistemic_mood_GRAMMAR:
      finally = TRUE;
      break;
    case deontic_mood_GRAMMAR:
      finally = TRUE;
      break;
    case realis_mood_GRAMMAR:
      finally = TRUE;
      break;
    default:
    printf("%s:%d:%s 0x%X previous_indicator, 0x%X tablet_indexFinger, 0x%X word \n", __FILE__, __LINE__, __FUNCTION__, previous_indicator, tablet_indexFinger, word_code);
    if (input_number > 0) {
      input_long+=sprintf(text+input_long, ", ");
    }
    slogan_argument_translate(previous_indicator, tablet_indexFinger, 
        tablet_long, tablet, &argument_long, text+input_long);
      input_long+= argument_long;
      argument_long = (uint16_t) *text_long - input_long;
      printf("%s:%d:%s 0x%X word_code, %s text \n", __FILE__, __LINE__, __FUNCTION__, word_code, text);
      ++input_number;
      break;
    }
    previous_indicator = phrase_indicator;
    if (finally == TRUE) {
      break;
    }
  }
}

void slogan_sort_array_establish(const uint8_t tablet_long, const v16us *tablet,
                          uint8_t *sort_array_long, uint64_t *sort_array) {
  assert(sort_array_long != NULL);
  assert(sort_array != NULL);
  assert(tablet != NULL);
  assert(tablet_long > 0);
  // algorithm
  // include cases, types, topic, and verb
  // include
  uint16_t tablet_indexFinger = 1;
  uint16_t indicator_list = 0;
  uint8_t indicator = 0;
  uint8_t exit = FALSE;
  uint16_t word = 0;
  uint8_t sort_array_indexFinger = 0;
  uint16_t quote_sort = 0;
  uint64_t phrase = 0;
  uint16_t phrase_place = 0;
  uint8_t phrase_long = 0;
  uint16_t maximum_tablet_long = tablet_long * TABLET_LONG;
  // uint8_t code_indexFinger = 0;
  // v8us quote_fill = {{0}};
  indicator_list = v16us_read(0, *tablet);
  indicator = (uint8_t)1 & indicator_list;
  // uint8_t indexFinger = 0;
  // printf("indicator %X\n", (uint) indicator);
  // printf("indicator_list %X\n", (uint)indicator_list);
  // printf("%s:%d:%s\n\t 0x%X word, 0x%X indexFinger 0x%lX phrase\n", __FILE__,
  // __LINE__,
  //   __FUNCTION__, word, tablet_indexFinger, phrase);
  for (tablet_indexFinger = 1; tablet_indexFinger < maximum_tablet_long;
       ++tablet_indexFinger) {
    // sort_array[sort_array_indexFinger] = 0;
    phrase = 0;
    //printf("%s:%d:%s\n\t 0x%X word, 0x%X indexFinger 0x%lX phrase\n", __FILE__,
    //       __LINE__, __FUNCTION__, word, tablet_indexFinger, (ulong)phrase);
    word = tablet_upcoming_grammar_or_quote_read(
        tablet_indexFinger, tablet_long, tablet, &tablet_indexFinger);
    // printf("%s:%d:%s\n\t 0x%X tablet_indexFinger\n", __FILE__, __LINE__,
    // __FUNCTION__, tablet_indexFinger);
    if (tablet_indexFinger > maximum_tablet_long) {
      printf("%s:%d:%s\n\t breaking\n", __FILE__, __LINE__, __FUNCTION__);
      break;
    }
    if ((word & QUOTE_DENOTE_MASK) == QUOTED_DENOTE) {
      quote_sort = word;
      continue;
    }
    // printf("%s:%d:%s\n\t 0x%X word, 0x%X indexFinger, 0x%lX phrase\n",
    // __FILE__, __LINE__,
    //    __FUNCTION__, word, tablet_indexFinger, phrase);
    // printf("word %04X SAI%X\n", word, sort_array_indexFinger);
    switch (word) {
    case topic_case_GRAMMAR:
      //verb_copy(tablet_indexFinger, tablet_long, tablet, &phrase);
      //phrase = (phrase << CODE_WORD_TIDBIT_LONG) | topic_case_GRAMMAR;
      //sort_array[sort_array_indexFinger] |= phrase;
      //// printf("topic case set %016lX\n",
      //// sort_array[sort_array_indexFinger]);
      //++sort_array_indexFinger;
      break;
    case nominative_case_GRAMMAR:
      //verb_copy(tablet_indexFinger, tablet_long, tablet, &phrase);
      //phrase = (phrase << CODE_WORD_TIDBIT_LONG) | nominative_case_GRAMMAR;
      //sort_array[sort_array_indexFinger] |= phrase;
      //// printf("topic case set %016lX\n",
      //// sort_array[sort_array_indexFinger]);
      //++sort_array_indexFinger;
      break;
    case realis_mood_GRAMMAR:
      //verb_copy(tablet_indexFinger, tablet_long, tablet, &phrase);
      //sort_array[sort_array_indexFinger] |= phrase;
      exit = TRUE;
      break;
    default:
      sort_array[sort_array_indexFinger] = (uint64_t)0;
      // printf("%s:%d\n\t 0x%X quote_sort, 0x%lX phrase\n", __FILE__, __LINE__,
      // quote_sort, phrase);
      if (quote_sort != 0) {
        sort_array[sort_array_indexFinger] = word;
        sort_array[sort_array_indexFinger] |=
            ((uint64_t)quote_sort << CODE_WORD_TIDBIT_LONG);
        // attach the grammar words if no quote sort
        quote_sort = 0;
      } else {
        phrase_quote_copy(tablet_indexFinger, tablet_long, tablet, &phrase);
        sort_array[sort_array_indexFinger] |= phrase;
      }
      printf("%s:%d:%s\n\t 0x%X quote_sort, 0x%lX phrase\n", __FILE__, __LINE__,
             __FUNCTION__, quote_sort, (ulong)phrase);
       printf("case detected %016lX\n",
       sort_array[sort_array_indexFinger]);
      ++sort_array_indexFinger;
      break;
    }

    if (exit == TRUE) {
      // load verb
      // printf("verb set %016lX\n", sort_array[sort_array_indexFinger]);
      ++sort_array_indexFinger;
      break;
    }
  }
  // get nominative case contents and put it at end.
  phrase_situate(tablet_long, tablet, nominative_case_GRAMMAR, &phrase_place,
                 &phrase_long);
  verb_copy(phrase_place+phrase_long-1, tablet_long, tablet, &phrase);
  //phrase = (phrase << CODE_WORD_TIDBIT_LONG) | nominative_case_GRAMMAR;
  sort_array[sort_array_indexFinger] |= phrase;
  // printf("topic case set %016lX\n",
  // sort_array[sort_array_indexFinger]);
  ++sort_array_indexFinger;

  // printf("exit %X\n", exit);
  sort_array_sort(sort_array_indexFinger, sort_array);
  *sort_array_long = sort_array_indexFinger;
}

void sort_array_tablet_translate(uint16_t sort_array_long, uint64_t *sort_array,
                                 v16us *sort_tablet) {
  assert(sort_tablet != NULL);
  assert(sort_array_long >= 0);
  assert(sort_array != NULL);
  uint16_t indexFinger = 0;
  uint16_t sort_indexFinger = 0;
  uint8_t tablet_indexFinger = 1;
  uint16_t code_word = 0;
  for (; indexFinger < sort_array_long; ++indexFinger) {
    for (sort_indexFinger = 3; sort_indexFinger < 4; --sort_indexFinger) {
      code_word =
          (uint16_t)(sort_array[indexFinger] >> (sort_indexFinger * 16));
      if (code_word > 0) {
        // printf("%s code_word 0x%X\n", __FILE__, code_word);
        v16us_write(tablet_indexFinger, code_word, sort_tablet);
        ++tablet_indexFinger;
        assert(tablet_indexFinger < TABLET_LONG);
      }
    }
  }
}

void sort_array_name_translate(const uint8_t sort_array_long, 
    const uint64_t *sort_array, uint8_t *name_long, char *name) {
  assert(sort_array_long > 0);
  assert(sort_array != NULL);
  assert(name_long != NULL);
  assert(*name_long > sort_array_long*WORD_LONG);
  assert(name != NULL);
  uint16_t indexFinger = 0;
  uint16_t sort_indexFinger = 0;
  uint8_t tablet_indexFinger = 1;
  uint16_t code_word = 0;
  //char word[WORD_LONG] = {0};
  const uint16_t max_name_long = *name_long;
  uint16_t word_long = max_name_long;
  uint16_t assigned_name_long = 0;
  //const uint16_t word_long_max = WORD_LONG;
  //uint16_t word_long_remains = WORD_LONG;
   //     printf("%s:%d:%s 0x%X code_word, %s word \n", __FILE__, __LINE__,
   //      __FUNCTION__, code_word);
  for (; indexFinger < sort_array_long; ++indexFinger) {
        printf("%s:%d:%s 0x%lX sort item\n", __FILE__, __LINE__,
         __FUNCTION__, sort_array[indexFinger] );
    for (sort_indexFinger = 3; sort_indexFinger < 4; --sort_indexFinger) {
        printf("%s:%d:%s 0x%X sort_indexFinger, 0x%lX\n", __FILE__, __LINE__,
         __FUNCTION__, sort_indexFinger,sort_array[indexFinger]  );
      code_word =
          (uint16_t)((uint64_t)(sort_array[indexFinger] >> (sort_indexFinger * 16)));
      if (code_word > 0) {
        // need quote translate for quotes
        assert(word_long >= WORD_LONG);
        word_code_translate(code_word, &word_long, name + assigned_name_long);
        assigned_name_long += word_long;
        word_long = max_name_long - assigned_name_long;
        //printf("%s:%d:%s 0x%X code_word, %d word_long, 0x%X assigned_name_long\n", __FILE__, __LINE__,
        // __FUNCTION__, code_word, word_long, assigned_name_long );
        //printf("%s:%d:%s 0x%X code_word, %d word_long, %s word, %s name\n", __FILE__, __LINE__,
        // __FUNCTION__, code_word, word_long, word, name);
        ++tablet_indexFinger;
        //word_long = word_long_max;
        //memset(word, 0, word_long);
        assert(tablet_indexFinger < TABLET_LONG);
      }
    }
  }
  *name_long = (uint8_t) assigned_name_long;
}

void ceremony_slogan_translate(const uint8_t tablet_long, const v16us *recipe,
                      uint16_t *produce_text_long, char *produce_text) {
  assert(tablet_long > 0);
  assert(recipe != NULL);
  assert(produce_text_long != NULL);
  assert(produce_text != NULL);
  const uint16_t max_produce_text_long = *produce_text_long;
  uint16_t text_long = 0;
  uint8_t sort_array_long = 16;
  uint64_t sort_array[16] = {0};
  uint8_t ceremony_name_long = 16*WORD_LONG;
  //char input_phrases[16*WORD_LONG] = {0};
  char ceremony_name[20*WORD_LONG] = {0};
  char input_arguments[20*WORD_LONG] = {0};
  uint16_t input_arguments_long = 20*WORD_LONG;
  // skipping topic 
  // make function word
  slogan_sort_array_establish(tablet_long, recipe, &sort_array_long, sort_array);
  printf("%s:%d:%s 0x%X sort_array_long\n", __FILE__, __LINE__, __FUNCTION__, sort_array_long);
  sort_array_name_translate(sort_array_long, sort_array, &ceremony_name_long, ceremony_name);
  // put in arguments, const for input, pointers for output
  printf("%s:%d:%s 0x%X ceremony_name_long\n", __FILE__, __LINE__, __FUNCTION__, ceremony_name_long);
  printf("%s:%d:%s %s ceremony_name\n", __FILE__, __LINE__, __FUNCTION__, ceremony_name);
  slogan_input_translate(tablet_long, recipe, &input_arguments_long, input_arguments);
  text_long += (uint16_t) sprintf(produce_text, "void %s(", ceremony_name);
  text_long += (uint16_t) sprintf(produce_text+text_long, "%s){\n", input_arguments);
  assert(text_long < max_produce_text_long);
  printf("%s:%d:%s %s ceremony_name, %s produce_text\n", __FILE__, __LINE__, 
      __FUNCTION__, ceremony_name, produce_text);
  *produce_text_long = text_long;
  //exit(19);
}
void import_translate(const uint8_t recipe_long, const v16us *recipe,
                      uint16_t *produce_text_long, char *produce_text) {
  assert(produce_text != NULL);
  assert(produce_text[0] == (char)0); // must be clean
  // get number quote from recipe
  // printf("%s:%d %s \n", __FILE__, __LINE__, __FUNCTION__);
  char name[WORD_LONG * 8] = {0};
  uint16_t name_long = WORD_LONG * 8;
  name_extract(recipe_long, recipe, accusative_case_GRAMMAR, &name_long, name);
  sprintf(produce_text, "#include \"%s.h\"\n", name);
  *produce_text_long = (uint16_t)strlen(produce_text);
}
void cycle_translate(const uint8_t recipe_long, const v16us *recipe,
                     uint16_t *produce_text_long, char *produce_text) {
  assert(produce_text != NULL);
  assert(produce_text[0] == (char)0); // must be clean
  // get variable from recipe
  uint16_t phrase_place = 0xF;
  uint8_t phrase_long = 0;
  printf("%s:%d:%s phrase_place 0x%X\n", __FILE__, __LINE__, __FUNCTION__,
         phrase_place);
  phrase_situate(recipe_long, recipe, ablative_case_GRAMMAR, &phrase_place,
                 &phrase_long);
  // printf("%s:%d phrase_place 0x%X\n", __FILE__, __LINE__, phrase_place);
  // tablet_print(recipe_long, recipe);
  uint16_t number_place = phrase_place + 1;
  const int ablativeCase_number = (int)tablet_upcoming_word_read(
      number_place, recipe_long, recipe, &number_place);
  phrase_place = 0xF;
  phrase_situate(recipe_long, recipe, instrumental_case_GRAMMAR, &phrase_place,
                 &phrase_long);
  printf("%s:%d:%s phrase_place 0x%X\n", __FILE__, __LINE__, __FUNCTION__,
         phrase_place);
  number_place = phrase_place + 1;
  const int instrumentalCase_number = (int)tablet_upcoming_word_read(
      number_place, recipe_long, recipe, &number_place);
  phrase_place = 0xF;
  phrase_situate(recipe_long, recipe, allative_case_GRAMMAR, &phrase_place,
                 &phrase_long);
  printf("%s:%d:%s phrase_place 0x%X\n", __FILE__, __LINE__, __FUNCTION__,
         phrase_place);
  number_place = phrase_place + 1;
  const int allativeCase_number = (int)tablet_upcoming_word_read(
      number_place, recipe_long, recipe, &number_place);
  uint16_t name_indexFinger = 0;
  char name[WORD_LONG * 8] = {0};
  const uint16_t name_long_maximum = WORD_LONG * 8;
  uint16_t name_remains = name_long_maximum;
  uint16_t name_long = 0;
  phrase_place = 0xF;
  phrase_situate(recipe_long, recipe, accusative_case_GRAMMAR, &phrase_place,
                 &phrase_long);
  printf("%s:%d phrase_place 0x%X\n", __FILE__, __LINE__, phrase_place);
  for (name_indexFinger = phrase_place;
       name_indexFinger < phrase_long + phrase_place - 1; ++name_indexFinger) {
    word_code_translate(tablet_upcoming_word_read(name_indexFinger,
                                                    recipe_long, recipe,
                                                    &name_indexFinger),
                          &name_remains, name + name_long);
    name_long += name_remains;
    name_remains = name_long_maximum - name_long;
  }
  sprintf(produce_text, "for(%s = 0x%X; %s < 0x%X; %s += 0x%X ){\n", name,
          ablativeCase_number, name, allativeCase_number, name,
          instrumentalCase_number);
  *produce_text_long = (uint16_t)strlen(produce_text);
}


void summon_input_translate(const uint16_t recipe_long, const v16us *recipe,
                              uint16_t *text_long, char *text) {
  assert(recipe != NULL);
  assert(text_long != NULL);
  assert(text != NULL);
  // for each grammatical case return the literal C values
  uint8_t recipe_indexFinger = 0;
  uint8_t tablet_indexFinger = 1;
  uint16_t indicator_list = 0;
  uint16_t indicator = 0;
  uint16_t word_code = 0;
  uint8_t previous_indicator = 0;
  uint8_t finally = FALSE;
  v16us tablet = {0};
  for (recipe_indexFinger = 0; recipe_indexFinger < recipe_long;
       ++recipe_indexFinger) {
    tablet = recipe[recipe_indexFinger];
    indicator_list = (uint16_t)tablet.s0;
    indicator = indicator_list & 1;
    for (tablet_indexFinger = 1; tablet_indexFinger < TABLET_LONG;
         ++tablet_indexFinger) {
      if (((indicator_list >> tablet_indexFinger) & 1) == indicator) {
        word_code = v16us_read(tablet_indexFinger, tablet);
        switch (word_code) {
        case epistemic_mood_GRAMMAR:
          finally = TRUE;
          break;
        case deontic_mood_GRAMMAR:
          finally = TRUE;
          break;
        case accusative_case_GRAMMAR:
          // grab quoted contents and output it appropriatly
          summon_argument_translate(previous_indicator, tablet_indexFinger, tablet,
                             text_long, text);
          break;
          default:
            printf("%s:%d:%s 0x%Xword_code ", __FILE__, __LINE__, __FUNCTION__, word_code);
        }
        previous_indicator = tablet_indexFinger;
      }
      if (finally == TRUE)
        break;
    }
  }
}
void quote_word_translate(uint16_t quote_code, uint16_t *text_long,
                          char *text) {
  assert(text_long != NULL);
  assert(*text_long > WORD_LONG);
  assert(text != NULL);
  // translate the quote sections and append them
  //const uint16_t pointed_tidbit =
  //    (quote_code & POINTED_MASK) >> POINTED_BEGIN;
  //const uint16_t series_tidbit =
  //    (quote_code & SERIES_MASK) >> SERIES_BEGIN;
  const uint16_t vector_thick =
      (quote_code & VECTOR_THICK_MASK) >> VECTOR_THICK_BEGIN;
  const uint16_t scalar_thick =
      (quote_code & SCALAR_THICK_MASK) >> SCALAR_THICK_BEGIN;
  const uint16_t sort_denote =
      (quote_code & SORT_DENOTE_MASK) >> SORT_DENOTE_BEGIN;
  uint16_t gross_text_long = 0;
  uint16_t vacancy_text_long = *text_long;
  uint16_t word_long = vacancy_text_long;
  printf("%s:%d:%s 0x%X sort_denote\n", __FILE__, __LINE__, __FUNCTION__, 
      sort_denote);
  assert(word_long >= WORD_LONG);
  switch (sort_denote) {
  case UINT_SORT_DENOTE:
    //word_code_translate(number_GRAMMAR, &word_long, text + gross_text_long);
    word_long = 0;
    break;
  case WORD_SORT_DENOTE:
    word_code_translate(word_GRAMMAR, &word_long, text + gross_text_long);
    break;
  case LETTER_SORT_DENOTE:
    word_code_translate(letter_GRAMMAR, &word_long, text + gross_text_long);
    break;
  default:
    assert(1 == 0);
    break;
  }
  quote_word_number_modernize;
  switch (vector_thick) {
  case VECTOR_THICK_2:
    word_code_translate(two_WORD, &word_long, text + gross_text_long);
    break;
  case VECTOR_THICK_1:
    word_long = 0;
    break;
  default:
    printf("%s:%d\t vector_thick 0x%X\n", __FILE__, __LINE__, vector_thick);
    assert(1 == 0);
    break;
  }
  quote_word_number_modernize;
  switch (scalar_thick) {
  case SIXTEEN_TIDBIT_SCALAR_THICK:
    word_code_translate(number_GRAMMAR, &word_long, text + gross_text_long);
    break;
  default:
    assert(1 == 0);
    break;
  }
  quote_word_number_modernize;
  *text_long = gross_text_long;
}
void code_name_derive(const uint8_t tablet_long, const v16us *tablet,
                      uint64_t *code_name) {
  // new derive_code_name function
  // get cases and quotes
  // get topic name along with topic
  // get verb
  // put each into a 32bit number
  // then sort the array of 32bit numbers with qsort
  // then get a 64bit hash of the array from hashlittle64 and return it
  assert(tablet_long != 0);
  assert(tablet != NULL);
  assert(code_name != NULL);
  uint8_t sort_array_long = 0;
  uint64_t sort_array[0x10] = {0};
  sort_array_establish(tablet_long, tablet, &sort_array_long, sort_array);
  // printf("sort_array_indexFinger %X\n", (uint64_t)sort_array_long);
  // printf("unsorted array %X %X %X\n", sort_array[0], sort_array[1],
  //      sort_array[2]);
  // sort_array_sort(sort_array_long, sort_array);
  // printf("sorted array %X %X %X\n", sort_array[0], sort_array[1],
  //       sort_array[2]);

  uint8_t indexFinger = 0;
  printf("%s:%d\tsort_array ", __FILE__, __LINE__);
  for (; indexFinger < sort_array_long; ++indexFinger) {
    printf("%016lX ", (ulong)sort_array[indexFinger]);
  }
  v16us_print(*tablet);
  printf("\n");
  *code_name = hash(sort_array_long, sort_array);
}

void tablet_translate(const uint8_t series_long, const v16us *tablet,
                      uint16_t *text_long, char *text) {
  assert(text != NULL);
  assert(*text == (char)0);
  assert(text_long != NULL);
  assert(*text_long >= TABLET_LONG * WORD_LONG);
  uint16_t word_code = 0;
  uint16_t gross_text_long = 0;
  uint16_t vacancy_text_long = *text_long;
  uint16_t word_long = 0;
  uint16_t tablet_indexFinger = 1;
  uint16_t maximum_tablet_long = TABLET_LONG * series_long;
  for (; tablet_indexFinger < maximum_tablet_long; ++tablet_indexFinger) {
    word_code = tablet_upcoming_word_read(tablet_indexFinger, series_long,
                                          tablet, &tablet_indexFinger);
    if (word_code == 0)
      continue;
    assert(vacancy_text_long > WORD_LONG);
    word_long = vacancy_text_long;
    printf("%s:%d:%s word_code 0x%X\n", __FILE__, __LINE__, __FUNCTION__, word_code);
    if ((word_code & QUOTE_DENOTE_MASK) == QUOTE_DENOTE) {
      quote_word_translate(word_code, &word_long, text + gross_text_long);
    } else {
      word_code_translate(word_code, &word_long, text + gross_text_long);
    }
    printf("%s:%d text %s\n", __FILE__, __LINE__, text);
    vacancy_text_long -= word_long;
    gross_text_long += word_long;
  }
  *text_long = gross_text_long;
}

uint8_t code_tablet_long_found(const uint16_t recipe_long,
                               const v16us *recipe) {

  uint16_t final_vector_indexFinger = 0;
  for (; final_vector_indexFinger < recipe_long; ++final_vector_indexFinger) {
    if ((recipe[final_vector_indexFinger].s0 & 1) == 1) {
      break;
    }
  }
  return (uint8_t)final_vector_indexFinger + 1;
}
uint16_t code_tablet_perspective_return(const uint8_t tablet_long,
                                        const v16us *tablet) {
  assert(tablet_long > 0 && tablet_long < MAX_INDEPENDENTCLAUSE_TABLET);
  assert(tablet != NULL);
  uint16_t indexFinger = 0;
  printf("%s:%d:%s\t  perspective \n", __FILE__, __LINE__, __FUNCTION__);
  return tablet_retrospective_grammar_read(tablet_long * (TABLET_LONG - 1),
                                           tablet_long, tablet, &indexFinger);
}

void ceremony_summon_translate(const uint8_t tablet_long, const v16us *recipe,
    uint16_t *produce_text_long, char *text) {
    uint8_t sort_array_long = 16;
    uint64_t sort_array[16] = {0};
    uint16_t word_long = 5;
  uint16_t vacant_text_long = *produce_text_long;
    v16us sort_tablet = {0};
    // establish ceremony name
    sort_array_establish(tablet_long, recipe, &sort_array_long, sort_array);
    sort_array_tablet_translate(sort_array_long, sort_array, &sort_tablet);
    tablet_translate(1, &sort_tablet, produce_text_long, text);
    // establish ceremony input
    text[*produce_text_long] = '(';
    ++*produce_text_long;
    // translate input arguments
    word_long = vacant_text_long;
    summon_input_translate(tablet_long, recipe, &word_long,
                             text + *produce_text_long);
    *produce_text_long += word_long;
    text[*produce_text_long] = ')';
    ++*produce_text_long;
    text[*produce_text_long] = ';';
    ++*produce_text_long;
    text[*produce_text_long] = '\n';
    ++*produce_text_long;
    printf("%s:%d text %s\n", __FILE__, __LINE__, text);
}

void code_opencl_translate(const uint16_t recipe_long, const v16us *recipe,
                           uint16_t *produce_text_long, char *text,
                           uint16_t *filename_long, char *filename,
                           uint16_t *file_sort) {
  assert(recipe_long > 0);
  assert(recipe != NULL);
  assert(filename != NULL);
  assert(text != NULL);
  assert(*text == (char)0);
  assert(filename_long != NULL);
  assert(file_sort != NULL);
  uint64_t code_number = 0;
  printf("%s:%d:%s\t 0x%X recipe_long\n", __FILE__, __LINE__, __FUNCTION__,
         recipe_long);
  code_name_derive((uint8_t)recipe_long, recipe, &code_number);
  printf("%s:%d:%s\tcode_number 0x%016lX \n", __FILE__, __LINE__, __FUNCTION__,
         (ulong)code_number);

  // if it ends in _deo or "ta", then translate to ceremony subpoena.
  // const uint16_t indicator_list = (uint16_t)recipe[0].s0;
  // const uint16_t indicator_referential = indicator_list & 1;
  // uint16_t indicator_indexFinger = 0;
  uint16_t perspective = 0;
  char word[WORD_LONG] = {0};
  uint16_t word_long = WORD_LONG;
  uint16_t topic_place = 0;
  uint8_t topic_long = 0;
  uint16_t topic_available = FALSE;
  uint16_t topic[4] = {0};
  // find the perspective or the grammatical mood
  // if index starts with  a 0 check next available tablet
  uint8_t tablet_long = code_tablet_long_found(recipe_long, recipe);
  // printf("%s:%d tablet_long 0x%X, recipe_long 0x%X\n", __FILE__, __LINE__,
  //       tablet_long, recipe_long);
  perspective = code_tablet_perspective_return(tablet_long, recipe);
  // find topic
  //
  phrase_situate(tablet_long, recipe, topic_case_GRAMMAR, &topic_place,
                 &topic_long);
  if (topic_place > 0) {
    topic_available = TRUE;
    topic[0] = tablet_read(topic_place, tablet_long, recipe);
  }
  printf("%s:%d:%s 0x%X perspective, 0x%X topic_available, 0x%X topic_long", 
      __FILE__, __LINE__, __FUNCTION__, perspective, topic_available, topic_long);

  if (perspective == epistemic_mood_GRAMMAR) { // comment
  } else if (perspective == deontic_mood_GRAMMAR) {
    ceremony_summon_translate(tablet_long, recipe, produce_text_long, text);
    return;
  }
  // if realis_mood with holy topic then is function declaration
  else if (perspective == realis_mood_GRAMMAR && topic_available == TRUE &&
           topic_long == 2 && topic[0] == holy_WORD) {
    printf("%s:%d:%s ceremony slogan found\n", __FILE__, __LINE__,
           __FUNCTION__);
    ceremony_slogan_translate(tablet_long, recipe, produce_text_long, text);
    return;
  } else {

  // else see if it is one of the special ones.
  switch (code_number) {
  case 0x1748480D9A863ADF:
    import_translate(tablet_long, recipe, produce_text_long, text);
    break;
  case 0x4271C4652D9F981E: // natural number
    natural_number_translate(tablet_long, recipe, produce_text_long, text);
    break;
  case 0x7C081E10958D2C07: // program topic, name nominative, begin, realis_mood
                           // filename set
    // set filename
    filename_establish(tablet_long, recipe, filename_long, filename);
    // set #include "pyac.h"
    language_include_establish(produce_text_long, text);
    break;
  case 0x29CE9B974C206F89: // cardinal topic, realis_mood
    cardinal_translate(produce_text_long, text, file_sort);
    break;
  case 0xCC2FBF6B9CA58D18: // for loop, cyclic topic, from, to, by, variable
    printf("%s:%d:%s\tcode_number 0x%016lX \n", __FILE__, __LINE__,
           __FUNCTION__, (ulong)code_number);
    cycle_translate(tablet_long, recipe, produce_text_long, text);
    break;
  case 0x958991171329E418: // number return
    // printf("text %X ", (uint) text[0]);
    return_translate(tablet_long, recipe, produce_text_long, text);
    break;
  case 0x19E250D690EC455B: // finally close curly braces
    text[0] = '}';
    text[1] = '\n';
    *produce_text_long = 2;
    break;
  default:
    word_code_translate((uint16_t)(*recipe).s3, &word_long, word);
    printf("%s:%d code_number 0x%lX recipe %s\n", __FILE__, __LINE__,
           (unsigned long)code_number, word);
    tablet_print((uint8_t)recipe_long, recipe);
    assert(1 == 0);
    *produce_text_long = 0;
  }
  }
}
